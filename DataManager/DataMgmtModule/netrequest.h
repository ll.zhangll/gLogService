﻿#ifndef NETREQUEST_H
#define NETREQUEST_H

#include "commdef.h"

class replyData
{
public:
    replyData(){nReplyType=0; data.clear();}
    ~replyData(){}

    int nReplyType;
    QByteArray data;
};

class NetRequest : public QObject
{
    Q_OBJECT
public:
    explicit NetRequest(QObject *parent = nullptr);

private:
    QNetworkAccessManager *m_pNetworkPost;

    QNetworkAccessManager *m_pNetworkGet;
    QMap<QNetworkReply*, replyData*> m_mapReply;
    QMutex *m_mutexNetGet;

    QTimer *m_timer;
    QFile  m_file;
signals:
    void SendReplyMessage(QString msg);
public slots:
    void doThreadWork(QString task);

private slots:
    void slotRecvNetFinished(QNetworkReply *reply);
    void slotNetGetReadyRead();
    void slotNetGetFinished();
    void slotNetGetError(QNetworkReply::NetworkError error);
};

#endif // NETREQUEST_H
