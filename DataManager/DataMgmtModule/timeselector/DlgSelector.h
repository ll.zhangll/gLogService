﻿#ifndef DLGSELECTOR_H
#define DLGSELECTOR_H

#include <QDialog>
#include <QDateTime>

class Selector;
class SelectorDateTime;

namespace Ui {
class DlgSelector;
}

class DlgSelector : public QDialog
{
    Q_OBJECT

public:
    explicit DlgSelector(QWidget *parent = 0);
    ~DlgSelector();

    void SetTime(QDateTime dateTime);
    void SetTitle(QString strTitle);
protected:
//    void paintEvent(QPaintEvent *);

signals:
    void sigClickedOk(QDateTime dateTime);
private slots:
    void on_btnCancel_clicked();
    void on_btnOk_clicked();

private:
    Ui::DlgSelector *ui;
};
/****************************************************************************************************************
 *
 *  时间选择器基础组合
 *
 *****************************************************************************************************************/
class SelectorDateTime : public QWidget
{
    Q_OBJECT
public:
    SelectorDateTime(QWidget *parent = 0);
private:
    int year;               //年份
    int month;              //月份
    int day;                //日期

    Selector *tumblerYear;   //年份选择器
    Selector *tumblerMonth;  //月份选择器
    Selector *tumblerDay;    //日期选择器
private slots:
    void currentValueChanged(const QString &);
public:
    //获取年份
    int getYear()           const;
    //获取月份
    int getMonth()          const;
    //获取日期
    int getDay()            const;
public Q_SLOTS:
    //设置年份
    void setYear(int year);
    //设置月份
    void setMonth(int month);
    //设置日期
    void setDay(int day);
    //设置年月日
    void setDateTime(int year, int month, int day);
};

/****************************************************************************************************************
 *
 *  时间选择器基础
 *
 *****************************************************************************************************************/
class Selector : public QWidget
{
    Q_OBJECT
public:
    Selector(QWidget *parent = 0);

protected:
    void wheelEvent(QWheelEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void paintEvent(QPaintEvent *);
    void drawBg(QPainter *painter);
    void drawLine(QPainter *painter);
    void drawText(QPainter *painter, int index, int offset);
private:
    QStringList listValue;          //值队列
    int currentIndex;               //当前索引
    QString currentValue;           //当前值
    bool horizontal;                //是否横向显示

    QColor foreground;              //前景色
    QColor background;              //背景色
    QColor lineColor;               //线条颜色
    QColor textColor;               //当前文本颜色

    int percent;                    //比例
    int offset;                     //偏离值
    bool pressed;                   //鼠标是否按下
    int pressedPos;                 //按下处坐标
    int currentPos;                 //当前值对应起始坐标
private:
    void checkPosition();
public:
    QStringList getListValue()      const;
    int getCurrentIndex()           const;
    QString getCurrentValue()       const;
    bool getHorizontal()            const;

    QColor getForeground()          const;
    QColor getBackground()          const;
    QColor getLineColor()           const;
    QColor getTextColor()           const;

    QSize sizeHint()                const;
    QSize minimumSizeHint()         const;
public Q_SLOTS:
    //设置值队列
    void setListValue(const QStringList &listValue);
    //设置当前索引
    void setCurrentIndex(int currentIndex);
    //设置当前值
    void setCurrentValue(const QString &currentValue);
    //设置横向显示,如果为假则纵向显示
    void setHorizontal(bool horizontal);
    //设置前景色
    void setForeground(const QColor &foreground);
    //设置背景色
    void setBackground(const QColor &background);
    //设置线条颜色
    void setLineColor(const QColor &lineColor);
    //设置文本颜色
    void setTextColor(const QColor &textColor);
Q_SIGNALS:
    void currentIndexChanged(int currentIndex);
    void currentValueChanged(QString currentValue);
};

#endif // DLGSELECTOR_H
