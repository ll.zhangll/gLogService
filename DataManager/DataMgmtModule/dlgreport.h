﻿#ifndef DLGREPORT_H
#define DLGREPORT_H

#include "commdef.h"
#include "timeselector/DlgSelector.h"
#include "DlgHint.h"

namespace Ui {
class DlgReport;
}

class DlgReport : public QDialog
{
    Q_OBJECT

public:
    explicit DlgReport(QWidget *parent = 0);
    ~DlgReport();

    void showDlgReport(uint LogInTime);
private:
    DlgSelector *m_pSliderTime;
    int   m_nFromChange;//1 frome 2 to
    QDateTime m_TmpDateTimeF; //即登陆的时间
    QDateTime m_TmpDateTimeT;
    int m_oldy1;
    bool m_bDownQuery;
    int nCurBillCount;
    void *m_pParent;
    /////////////
    QDateTime m_tmpReprotDMTime;
    //
    DlgHint *m_pDlgHint;
    qint64 m_tmpDisableSecond;
protected:
    bool eventFilter(QObject *obj, QEvent *event);
public slots:
    void slotReportTimeChanged(QDateTime dateTime);
    void on_btnReportReturn_clicked();
    void on_btnReportQuery_clicked();
    void slotReportLogsListResult(QString data);
    void slotReportInfoListResult(QString data);
    void slotReportQrCodeResult(QString data);
private slots:
    void on_btnReportQueryDay_clicked();
    void on_btnReportQueryMonth_clicked();
    void on_btnReportExport_clicked();

private:
    Ui::DlgReport *ui;
};

#endif // DLGREPORT_H
