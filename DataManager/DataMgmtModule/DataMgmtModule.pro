#-------------------------------------------------
#
# Project created by QtCreator 2019-07-25T11:00:29
#
#-------------------------------------------------

QT       += core gui network

CONFIG  += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DataMgmtModule
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        dlgmain.cpp \
    netrequest.cpp \
    dlgeditdish.cpp \
    keyboard/arrowbubble.cpp \
    keyboard/homekeyboard.cpp \
    keyboard/keyboard.cpp \
    dlgcurfoodfeature.cpp \
    dlgeditmenu.cpp \
    dlgbill.cpp \
    timewidget/mytimedialog.cpp \
    timewidget/tumbler.cpp \
    timewidget/tumblerdatetime.cpp \
    dlgreport.cpp \
    timeselector/DlgSelector.cpp \
    DlgHint.cpp

HEADERS += \
        dlgmain.h \
    netrequest.h \
    dlgeditdish.h \
    keyboard/arrowbubble.h \
    keyboard/homekeyboard.h \
    keyboard/keyboard.h \
    commdef.h \
    dlgcurfoodfeature.h \
    dlgeditmenu.h \
    dlgbill.h \
    timewidget/mytimedialog.h \
    timewidget/tumbler.h \
    timewidget/tumblerdatetime.h \
    dlgreport.h \
    timeselector/DlgSelector.h \
    DlgHint.h

FORMS += \
        dlgmain.ui \
    dlgeditdish.ui \
    keyboard/keyboard.ui \
    dlgcurfoodfeature.ui \
    dlgeditmenu.ui \
    dlgbill.ui \
    timewidget/mytimedialog.ui \
    dlgreport.ui \
    timeselector/DlgSelector.ui \
    DlgHint.ui

CONFIG(debug, debug|release){
    DESTDIR = ../bin
}
else {
    DESTDIR = ../bin
}

RESOURCES += \
    dataMgmtMoudle.qrc
