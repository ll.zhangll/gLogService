﻿#ifndef DLGCURFOODFEATURE_H
#define DLGCURFOODFEATURE_H

#include "commdef.h"

namespace Ui {
class DlgCurFoodFeature;
}

class DlgCurFoodFeature : public QDialog
{
    Q_OBJECT

public:
    explicit DlgCurFoodFeature(QWidget *parent = 0);
    ~DlgCurFoodFeature();

    void SetShowData(QJsonArray array, QString tmpDisName);
    void SetPParent(void *parent);

private:
    QJsonArray m_CurArray;
    QString m_CurFoodName;
    int m_countNum;
    void *m_pParent;

    int m_xPressed;
    bool m_bIsSlide;
    int m_oldy;//鼠标的Y坐标
signals:
    void sigbLastPage(bool bLast);
private slots:
    void FirstPage();
    void bLastPage(bool bLast);
protected:
    bool eventFilter(QObject *obj, QEvent *event);
private slots:
    void on_btnAddfoodFinish_clicked();
    void on_btnAddfoodDeleteCur_clicked();

private:
    Ui::DlgCurFoodFeature *ui;
};

#endif // DLGCURFOODFEATURE_H
