#ifndef MYTIMEDIALOG_H
#define MYTIMEDIALOG_H
#include <QDateTime>
#include <QDialog>
#include "tumblerdatetime.h"
namespace Ui {
class MyTimeDialog;
}

class MyTimeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MyTimeDialog(QWidget *parent = 0);
    ~MyTimeDialog();
    void SetTime(QDateTime dateTime);
    void SetTitle(QString strTitle);
private slots:
    void on_pushButtonTimeOK_clicked();
    void on_pushButtonTimeCancel_clicked();
signals:
    void ClickedOK(QDateTime dateTime);
private:
    Ui::MyTimeDialog *ui;
protected:
    void paintEvent(QPaintEvent *);
};

#endif // MYTIMEDIALOG_H
