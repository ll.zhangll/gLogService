﻿#include "mytimedialog.h"
#include "ui_mytimedialog.h"
#include <QPainter>

MyTimeDialog::MyTimeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MyTimeDialog)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Popup|Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint | Qt::Dialog);
    ui->labTimeTitle->setText("");
    ui->labTimeTitle->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
}

MyTimeDialog::~MyTimeDialog()
{
    delete ui;
}

void MyTimeDialog::SetTime(QDateTime dateTime)
{
    ui->sliderDateTime->setDateTime(dateTime.date().year(), dateTime.date().month(),
                                    dateTime.date().day(), dateTime.time().hour(),
                                    dateTime.time().minute(), dateTime.time().second());
}

void MyTimeDialog::SetTitle(QString strTitle)
{
    ui->labTimeTitle->setText(strTitle);
}

void MyTimeDialog::on_pushButtonTimeOK_clicked()
{
    QDateTime dateTime;
    QDate date;
    QTime time;

    date.setDate(ui->sliderDateTime->getYear(), ui->sliderDateTime->getMonth(), ui->sliderDateTime->getDay());
    time.setHMS(ui->sliderDateTime->getHour(),ui->sliderDateTime->getMin(),ui->sliderDateTime->getSec());
    dateTime.setDate(date);
    dateTime.setTime(time);
    emit ClickedOK(dateTime);
    close();
}

void MyTimeDialog::on_pushButtonTimeCancel_clicked()
{
    close();
}

void MyTimeDialog::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.fillRect(this->rect(), QColor(169,169,169,220));
}
