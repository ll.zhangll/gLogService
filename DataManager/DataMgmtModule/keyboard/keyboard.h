﻿#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <QWidget>
#include <QMap>
#include <QTimer>
#include <QTextCodec>
#include "arrowbubble.h"

class QLabel;
class QLineEdit;
class QComboBox;
class QTextEdit;
class QMenu;

namespace Ui {
class keyboard;
}

class keyboard : public QWidget
{
    Q_OBJECT
public:
    static keyboard* Instance() {
        if(!_instance){
            _instance = new keyboard;
        }
        return _instance;
    }
public:
    explicit keyboard(QWidget *parent = 0);
    ~keyboard();

    void TransFormInput(bool bChinese);
private:
    void InitForm();    //初始化窗体数据
    void InitProperty();//初始化属性
    void changeType(QString type);  //改变输入法类型
    void changeLetter(bool isUpper);//改变字母大小写
    void clearInputName(bool bAll=false);
    void InputTransform();
    QString GetListPinyins(const QString &text);
    QString GetPinyinByChar(int code, QString strZimu_Shuzi);

    void TransFormInputPri();

    QPoint m_mousePoint;              //鼠标拖动自定义标题栏时的坐标
    bool mousePressed;              //鼠标是否按下

    static keyboard *_instance;
    QLineEdit * currentLineEdit;    //当前焦点的文本框
    QTextEdit * currentTextEdit;
    QString currentType;            //当前输入法类型

    QMap<QByteArray, QByteArray> m_mapHanziString;
    QMap<QByteArray, QByteArray> m_mapDishesString;
    QString m_InputText;
    //
    arrowBubble *m_bubble;
    QTimer m_timer;
    //
    QTextCodec *m_utf8;
    QTextCodec *m_gbk;
    //
    QString m_outTextTmp;
    int m_tmpPageCount;
    QStringList m_dishesMatchListTmp;
    int m_tmpPageComboCount;
    QWidget *m_WinId;
protected:
    bool eventFilter(QObject *obj, QEvent *event);
    void mouseMoveEvent(QMouseEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *);

private slots:
    void slotBtnClicked(); //按键处理
private:
    Ui::keyboard *ui;
};

#endif // KEYBOARD_H
