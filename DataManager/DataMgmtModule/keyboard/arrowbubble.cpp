﻿#include "arrowbubble.h"
#include <QBoxLayout>
#include <QPainter>

arrowBubble::arrowBubble(QWidget *parent) : QWidget(parent)
  ,m_startX(50)
  ,m_triangleWidth(TRIANGLE_WIDTH)
  ,m_triangleHeight(TRIANGLE_HEIGHT)
{
    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_TranslucentBackground);
    //设置阴影边框;
    m_shadowEffect = new QGraphicsDropShadowEffect(this);
    m_shadowEffect->setOffset(0, 0);
    m_shadowEffect->setColor(Qt::gray);
    m_shadowEffect->setBlurRadius(SHADOW_WIDTH);
    this->setGraphicsEffect(m_shadowEffect);

    m_lbText = new QLabel(this);
    m_lbText->setAlignment(Qt::AlignCenter);
    QFont font;
    font.setFamily(u8"黑体");
    font.setPointSize(16);
    font.setUnderline(true);
    m_lbText->setFont(QFont(font));

    QHBoxLayout* hMainLayout = new QHBoxLayout(this);
    hMainLayout->addWidget(m_lbText);
    hMainLayout->setSpacing(0);
    hMainLayout->setContentsMargins(SHADOW_WIDTH, SHADOW_WIDTH + TRIANGLE_HEIGHT, SHADOW_WIDTH, SHADOW_WIDTH);
}

void arrowBubble::setStartPos(int startX)
{
    m_startX = startX;
}

void arrowBubble::setTriangleInfo(int width, int height)
{
    m_triangleWidth = width;
    m_triangleHeight = height;
}

void arrowBubble::setText(QString s)
{
    m_lbText->setText(s);
}

void arrowBubble::setDerection(arrowBubble::Derection d)
{
    derect = d;
}

QString arrowBubble::text()
{
    return m_lbText->text();
}

void arrowBubble::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setPen(Qt::NoPen);
    painter.setBrush(QColor(255, 255, 255, 200));
    // 小三角区域;
    QPainterPath drawPath;
    QPolygon trianglePolygon;
    switch (derect)
    {
        case up:
        trianglePolygon << QPoint(m_startX, m_triangleHeight + SHADOW_WIDTH);
        trianglePolygon << QPoint(m_startX + m_triangleWidth / 2, SHADOW_WIDTH);
        trianglePolygon << QPoint(m_startX + m_triangleWidth, m_triangleHeight + SHADOW_WIDTH);
        drawPath.addRoundedRect(QRect(SHADOW_WIDTH, m_triangleHeight + SHADOW_WIDTH,\
                                    width() - SHADOW_WIDTH * 2, height() - SHADOW_WIDTH * 2 - m_triangleHeight),\
                                    BORDER_RADIUS, BORDER_RADIUS);
        break;
        case left:
        trianglePolygon << QPoint(3 + m_triangleHeight, 43);
        trianglePolygon << QPoint(8, 48 + m_triangleWidth / 2);
        trianglePolygon << QPoint(3 + m_triangleHeight, 39 + m_triangleWidth);
        drawPath.addRoundedRect(QRect(SHADOW_WIDTH, m_triangleHeight + SHADOW_WIDTH,\
                                    width() - SHADOW_WIDTH * 2, height() - SHADOW_WIDTH * 2 - m_triangleHeight),\
                                    BORDER_RADIUS, BORDER_RADIUS);
        break;
        case right:
        trianglePolygon << QPoint(115, 40);
        trianglePolygon << QPoint(116 + m_triangleHeight, 48 + m_triangleWidth / 2);
        trianglePolygon << QPoint(115, 39 + m_triangleWidth);
        drawPath.addRoundedRect(QRect(SHADOW_WIDTH, m_triangleHeight + SHADOW_WIDTH,\
                                    width() - SHADOW_WIDTH * 2, height() - SHADOW_WIDTH * 2 - m_triangleHeight),\
                                    BORDER_RADIUS, BORDER_RADIUS);

        break;
        case down:
        trianglePolygon << QPoint(m_startX, 65);
        trianglePolygon << QPoint(m_startX + m_triangleWidth / 2, 65 + m_triangleHeight);
        trianglePolygon << QPoint(m_startX + m_triangleWidth, 65);
        drawPath.addRoundedRect(QRect(SHADOW_WIDTH, m_triangleHeight + SHADOW_WIDTH,\
                                    width() - SHADOW_WIDTH * 2, height() - SHADOW_WIDTH * 2 - m_triangleHeight),\
                                    BORDER_RADIUS, BORDER_RADIUS);
        break;
    default: break;
    }

    drawPath.addPolygon(trianglePolygon);
    painter.drawPath(drawPath);
}
