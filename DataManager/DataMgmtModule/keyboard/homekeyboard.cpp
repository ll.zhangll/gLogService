﻿#include "homekeyboard.h"

#include <QMouseEvent>
#include <QPainter>
#include "keyboard.h"

HomeKeyboard::HomeKeyboard(QWidget *parent) : QWidget(parent)
{
    this->setWindowFlags(Qt::Tool | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    this->setAttribute(Qt::WA_ShowWithoutActivating , true);
    this->setAttribute(Qt::WA_TranslucentBackground);

    this->setMaximumSize(50,50);
    this->setMinimumSize(50,50);

    this->setAutoFillBackground(true);
    m_Pos = 0;
    m_mouseMoved = false;
}

void HomeKeyboard::mouseMoveEvent(QMouseEvent *e)
{
    if (m_mousePressed && (e->buttons() && Qt::LeftButton))
    {
        this->move(e->globalPos() - m_mousePoint);
        e->accept();
        m_mouseMoved = true;
    }
}

void HomeKeyboard::mousePressEvent(QMouseEvent *e)
{
    if (e->button() == Qt::LeftButton)
    {
        m_mousePressed = true;
        m_mousePoint = e->globalPos() - this->pos();
        e->accept();
    }
}

void HomeKeyboard::mouseReleaseEvent(QMouseEvent *e)
{
    m_mousePressed = false;
    if(!m_mouseMoved)
    {
        if(keyboard::Instance()->isVisible())
        {
            keyboard::Instance()->setVisible(false);
        }
        else
        {
            keyboard::Instance()->setVisible(true);
        }
    }
    else
    {

    }
    m_mouseMoved = false;
}

void HomeKeyboard::mouseDoubleClickEvent(QMouseEvent *)
{

}

void HomeKeyboard::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    p.drawPixmap(0, 0 , QPixmap(":/keyboard/image/keyboard/Home_Btn.png"));
}

void HomeKeyboard::closeEvent(QCloseEvent *event)
{

}
