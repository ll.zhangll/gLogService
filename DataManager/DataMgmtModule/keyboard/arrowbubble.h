﻿#ifndef ARROWBUBBLE_H
#define ARROWBUBBLE_H

#include <QWidget>
#include <QGraphicsDropShadowEffect>
#include <QLabel>

#define SHADOW_WIDTH 15                 // 窗口阴影宽度;
#define TRIANGLE_WIDTH 15               // 小三角的宽度;
#define TRIANGLE_HEIGHT 10              // 小三角的高度;
#define BORDER_RADIUS 5                 // 窗口边角的弧度;

class arrowBubble : public QWidget
{
    Q_OBJECT
public:
    explicit arrowBubble(QWidget *parent = nullptr);
    enum Derection{
        left,
        right,
        up,
        down
    };
    // 设置小三角起始位置;
    void setStartPos(int startX);
    // 设置小三角宽和高;
    void setTriangleInfo(int width, int height);
    void setText(QString s);
    void setDerection(Derection d);
    QString text();
    QGraphicsDropShadowEffect* m_shadowEffect;
    QLabel *m_lbText;
protected:
    void paintEvent(QPaintEvent *);
private:
    // 小三角起始位置;
    int m_startX;
    // 小三角的宽度;
    int m_triangleWidth;
    // 小三角高度;
    int m_triangleHeight;
    Derection derect;
};

#endif // ARROWBUBBLE_H
