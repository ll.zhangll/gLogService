﻿#ifndef HOMEKEYBOARD_H
#define HOMEKEYBOARD_H

#include <QWidget>

class HomeKeyboard : public QWidget
{
    Q_OBJECT
public:
    explicit HomeKeyboard(QWidget *parent = nullptr);
    bool m_mousePressed;
    bool m_mouseMoved;
    QPoint m_mousePoint;//鼠标拖动自定义标题栏时的坐标

private:
    void mouseMoveEvent(QMouseEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void mouseDoubleClickEvent(QMouseEvent *);
    void paintEvent(QPaintEvent *event);
    void closeEvent(QCloseEvent *event);

    int m_Pos;
};

#endif // HOMEKEYBOARD_H
