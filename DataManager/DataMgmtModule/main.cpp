﻿#include "dlgmain.h"
#include <QApplication>

quint16 g_nscreenwidth = 1920;
quint16 g_nscreenheight = 1080;

QRect g_FirstScreen = QRect(0,0,1920,1080);
QString g_Url = "http://127.0.0.1";
QString g_MasterIP = "http://127.0.0.1";
QString g_CurMasterQrCodeIp;

int main(int argc, char *argv[])
{
    qDebug() << "DataMgmtDodule start";
    QApplication a(argc, argv);

    qint64 nn = QDateTime::currentMSecsSinceEpoch();

    double tmpMoney = 0.3;
    int nTotalMoney;
    nTotalMoney += tmpMoney*100;
    qDebug() << nTotalMoney;


    uint gLogInTime = 0;
    QString WhichOne;
    QStringList arguments = QCoreApplication::arguments();
    if(arguments.size() > 4){
        g_MasterIP = arguments[1];
        g_FirstScreen.setX(QString(arguments[2]).toInt());
        g_FirstScreen.setY(QString(arguments[3]).toInt());
        WhichOne = arguments[4];
        if(g_Url.length() < 1){
            g_Url = "http://127.0.0.1";
        }
    }

    DlgMain w;
    w.show();
    if("MenuCfg" == WhichOne){
        g_Url = QString("%1:9001").arg(g_MasterIP);
        w.StartMenuCfg();
    }else if("MenuTemplateCfg" == WhichOne){
        g_Url = QString("%1:9001").arg(g_MasterIP);
        w.StartMenuTemplate();
    }else if("Bill" == WhichOne){
//        g_Url = QString("%1:10001").arg(g_MasterIP);
        g_Url = QString("http://127.0.0.1:10001");
//        gLogInTime = QString(arguments[5]).toUInt();
        //改为默认从当天0点0分0秒开始查询
        QDateTime currentDate = QDateTime::currentDateTime();
        QDateTime zeroDate;
        zeroDate.setDate(QDate(currentDate.date().year(),currentDate.date().month(),currentDate.date().day()));
        zeroDate.setTime(QTime(0,0,0));
        uint zeroTime = zeroDate.toTime_t();
        w.StartBill(zeroTime);
    }else if("Report" == WhichOne){
//        g_Url = QString("%1:10001").arg(g_MasterIP);
        g_Url = QString("http://127.0.0.1:10001");
        gLogInTime = QString(arguments[5]).toUInt();
        g_CurMasterQrCodeIp = arguments[6];
        g_CurMasterQrCodeIp = g_CurMasterQrCodeIp.append(":10001");
        qDebug() << "-=------g_CurMasterQrCodeIp---------" << g_CurMasterQrCodeIp;
        w.StartReport(gLogInTime);
    }else{
//        g_Url = QString("%1:9001").arg(g_MasterIP);
//        w.StartMenuCfg();

//        g_Url = QString("%1:10001").arg(g_MasterIP);
//        w.StartReport(0);

        g_Url = QString("%1:9001").arg(g_MasterIP);
        w.StartMenuTemplate();

//        g_Url = QString("http://127.0.0.1:10001");
//        //改为默认从当天0点0分0秒开始查询
//        QDateTime currentDate = QDateTime::currentDateTime();
//        QDateTime zeroDate;
//        zeroDate.setDate(QDate(currentDate.date().year(),currentDate.date().month(),currentDate.date().day()));
//        zeroDate.setTime(QTime(0,0,0));
//        uint zeroTime = zeroDate.toTime_t();
//        w.StartBill(zeroTime);
    }

    qDebug() << "DataMgmtDodule=" << g_Url << "WhichOne=" << WhichOne;
    qDebug() << "启动外部进程2=" << QDateTime::currentMSecsSinceEpoch() - nn;

    return a.exec();
}
