﻿#include "DlgHint.h"
#include "ui_DlgHint.h"

DlgHint::DlgHint(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgHint)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);
    this->setFixedSize(295,346);
    ui->labelQrCode->setScaledContents(true);
}

DlgHint::~DlgHint()
{
    delete ui;
}

void DlgHint::SetQrCode(QPixmap &pixmap)
{
    ui->labelQrCode->setPixmap(pixmap);
}

void DlgHint::on_btnReturn_clicked()
{
    this->close();
}
