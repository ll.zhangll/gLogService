﻿#include "dlgeditdish.h"
#include "ui_dlgeditdish.h"
#include "dlgmain.h"

DlgEditDish::DlgEditDish(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgEditDish)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint | Qt::Dialog);
    this->setFixedSize(g_nscreenwidth, g_nscreenheight);
    this->setGeometry(g_FirstScreen);

    m_pParent = (void*)parent;
    m_oldy1 = 0;
    m_tmpTmpPop = NULL;
    m_tmpDlgPopUp = NULL;
    m_tmpDlgCurFoodFeaMgmt = NULL;
    //
    ui->listWidget_DishCfg->viewport()->installEventFilter(this);
    ui->listWidget_DishCfg->verticalScrollBar()->installEventFilter(this);
    connect(ui->listWidget_DishCfg,&QListWidget::itemClicked,this,&DlgEditDish::slotListWidgetDishCfgitemClicked);
    ui->listWidget_DishCfg->setSortingEnabled(true);
    //
    ui->lineEdit_SearchDishNameShow->setPlaceholderText(u8"搜索菜品");
    connect(ui->lineEdit_SearchDishNameShow,&QLineEdit::textChanged,this,&DlgEditDish::slotSearchDishtextChanged);

//    ui->pushButton_deleteCurDish->setFocusPolicy(Qt::NoFocus);
//    ui->pushButton_curDishFeature->setFocusPolicy(Qt::NoFocus);
//    ui->pushButton_curDishSave->setFocusPolicy(Qt::NoFocus);
}

DlgEditDish::~DlgEditDish()
{
    delete ui;
}

bool DlgEditDish::eventFilter(QObject *obj, QEvent *event)
{
    if(obj == ui->listWidget_DishCfg->viewport()){
        if(QEvent::MouseButtonPress == event->type()){
            QMouseEvent *move = static_cast<QMouseEvent *>(event);
            m_oldy1 = move->pos().y();
        }
        if(QEvent::MouseButtonRelease == event->type()){
        }
        if(QEvent::MouseMove == event->type()){
            QMouseEvent *move = static_cast<QMouseEvent *>(event);
            int newy1 = move->pos().y();
            int nCurrentVal = ui->listWidget_DishCfg->verticalScrollBar()->value();
            if(abs(newy1-m_oldy1) >= 45){
                if(newy1 > m_oldy1) {
                    //up
                    ui->listWidget_DishCfg->verticalScrollBar()->setValue(nCurrentVal-1);
                } else if(newy1 < m_oldy1) {
                    //down
                    ui->listWidget_DishCfg->verticalScrollBar()->setValue(nCurrentVal+1);
                }
                m_oldy1 = newy1;
            }
        }
    }
    return QDialog::eventFilter(obj,event);
}

void DlgEditDish::slotMenuAll()
{
    ui->listWidget_DishCfg->clear();
    ui->lineEdit_dishPriceShow->clear();
    ui->lineEdit_dishNameShow->clear();
    ui->lineEdit_dishEnergyShow->clear();
    ui->lineEdit_dishSugarShow->clear();
    ui->lineEdit_dishFatShow->clear();
    ui->lineEdit_dishProteinShow->clear();
    m_tmpDishName.clear();
    ui->pushButton_curDishFeature->setDisabled(false);

    QMapIterator<QString, FoodDetails*> iter_traversal(g_FoodDetailsList);
    while (iter_traversal.hasNext()) {
        iter_traversal.next();

        ui->listWidget_DishCfg->addItem(iter_traversal.key());
    }
}

void DlgEditDish::slotEnergy(QString data)
{
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8(), &error);
    QJsonObject obj = doc.object();
    QJsonArray array = obj.take("Foods").toArray();
    if(array.count() > 0){
        QJsonObject child = array.at(0).toObject();
        ui->lineEdit_dishEnergyShow->setText(child.take("Hot").toString());
        ui->lineEdit_dishSugarShow->setText(child.take("Sugar").toString());
        ui->lineEdit_dishFatShow->setText(child.take("Fat").toString());
        ui->lineEdit_dishProteinShow->setText(child.take("Protein").toString());
    }
}

void DlgEditDish::slotFoodUrlArray(QJsonArray array)
{
    //收到某菜品的特征库信息后
    ui->pushButton_curDishFeature->setDisabled(false);
    //特征库管理界面
    if(NULL == m_tmpDlgCurFoodFeaMgmt){
        m_tmpDlgCurFoodFeaMgmt = new DlgCurFoodFeature(this);
        m_tmpDlgCurFoodFeaMgmt->SetPParent(m_pParent);
    }
    m_tmpDlgCurFoodFeaMgmt->SetShowData(array, m_tmpDishName);
    m_tmpDlgCurFoodFeaMgmt->show();
}

void DlgEditDish::slotListWidgetDishCfgitemClicked(QListWidgetItem *item)
{
    m_tmpDishName = item->text();
    QMap<QString, FoodDetails*>::const_iterator IterFind = g_FoodDetailsList.find(m_tmpDishName);
    if (IterFind != g_FoodDetailsList.end() && IterFind.key() == m_tmpDishName) {
        FoodDetails* p = IterFind.value();

        ui->lineEdit_dishPriceShow->setText(QString::number((double)p->nPrice/100,'f',2));
        ui->lineEdit_dishNameShow->setText(p->AnotherName);

        ui->lineEdit_dishEnergyShow->clear();
        ui->lineEdit_dishSugarShow->clear();
        ui->lineEdit_dishFatShow->clear();
        ui->lineEdit_dishProteinShow->clear();

        DlgMain *parent = (DlgMain*)m_pParent;
        parent->GetFoodEnergy(m_tmpDishName);
    }
}

void DlgEditDish::slotSearchDishtextChanged(const QString &text)
{
    ui->listWidget_DishCfg->clear();
    ui->lineEdit_dishNameShow->clear();
    ui->lineEdit_dishPriceShow->clear();
    ui->lineEdit_dishEnergyShow->clear();
    ui->lineEdit_dishFatShow->clear();
    ui->lineEdit_dishSugarShow->clear();
    ui->lineEdit_dishProteinShow->clear();

    if(text.length() > 0){
        QMapIterator<QString, FoodDetails*> iter_traversal(g_FoodDetailsList);
        while (iter_traversal.hasNext()) {
            iter_traversal.next();
            QString name = iter_traversal.key();
            if(name.contains(text)){
                ui->listWidget_DishCfg->addItem(name);
            }
        }
    }else{
        QMapIterator<QString, FoodDetails*> iter_traversal(g_FoodDetailsList);
        while (iter_traversal.hasNext()) {
            iter_traversal.next();
            ui->listWidget_DishCfg->addItem(iter_traversal.key());
        }
    }
}

void DlgEditDish::on_pushButton_curDishSave_clicked()
{
    QJsonObject obj;
    obj.insert("Cmd", "editFoodEnergy");
    obj.insert("Name", m_tmpDishName);
    obj.insert("Hot", ui->lineEdit_dishEnergyShow->text());
    obj.insert("Sugar", ui->lineEdit_dishSugarShow->text());
    obj.insert("Fat", ui->lineEdit_dishFatShow->text());
    obj.insert("Protein", ui->lineEdit_dishProteinShow->text());
    QJsonDocument doc;
    doc.setObject(obj);
    QByteArray array = doc.toJson();
    DlgMain *parent = (DlgMain*)m_pParent;
    parent->SetFoodEnergy(QString(array));

    QJsonObject objBase;
    objBase.insert("Cmd", "editFoodInfo");
    objBase.insert("Name", m_tmpDishName);
    objBase.insert("Price", ui->lineEdit_dishPriceShow->text().toDouble()*100);
    objBase.insert("OtherName", ui->lineEdit_dishNameShow->text());
    QJsonDocument docBase;
    docBase.setObject(objBase);
    QByteArray arrayBase = docBase.toJson();
    parent->SetFoodInfo(QString(arrayBase));

    /////////////////////////////////////
    QMap<QString, FoodDetails*>::const_iterator IterFind = g_FoodDetailsList.find(m_tmpDishName);
    if (IterFind != g_FoodDetailsList.end() && IterFind.key() == m_tmpDishName) {
        FoodDetails* p = IterFind.value();
        p->nPrice = ui->lineEdit_dishPriceShow->text().toDouble()*100;
        p->AnotherName = ui->lineEdit_dishNameShow->text();
    }

///////////////////////
    if(NULL != m_tmpTmpPop){
        delete m_tmpTmpPop;
        m_tmpTmpPop = NULL;
    }
    m_tmpTmpPop = new QDialog(this);
    m_tmpTmpPop->setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);
    m_tmpTmpPop->setFixedSize(300,200);

    QLabel *label = new QLabel(m_tmpTmpPop);
    label->setText(QString("请稍后..."));
    label->setFixedSize(300,100);

    label->setAlignment(Qt::AlignCenter);

    m_tmpTmpPop->setStyleSheet("QDialog{background-color:rgba(247,212,136); border:1px; border-radius:10px;}");
    label->setStyleSheet("QLabel{font-size:35px; font-family:\"黑体\";}");

    QHBoxLayout *Htmp = new QHBoxLayout(m_tmpTmpPop);
    Htmp->addStretch(0);
    Htmp->addWidget(label);
    Htmp->addStretch(0);
    Htmp->setSpacing(0);
    Htmp->setMargin(0);

    QTimer timer;
    connect(&timer,&QTimer::timeout,[=](){
        m_tmpTmpPop->close();
    });
    timer.setSingleShot(true);
    timer.start(1000);

    m_tmpTmpPop->exec();
}

void DlgEditDish::on_pushButton_deleteCurDish_clicked()
{
    //删除菜品
    if(NULL != m_tmpDlgPopUp){
        delete m_tmpDlgPopUp;
        m_tmpDlgPopUp = NULL;
    }
    m_tmpDlgPopUp = new QDialog(this);
    m_tmpDlgPopUp->setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);
    m_tmpDlgPopUp->setFixedSize(600,400);

    QLabel *label = new QLabel(m_tmpDlgPopUp);
    label->setText(QString("确认要删除%1吗?").arg(m_tmpDishName));
    QPushButton *btnOk = new QPushButton("确 定", m_tmpDlgPopUp);
    QPushButton *btnCancel = new QPushButton("取 消", m_tmpDlgPopUp);
    btnOk->setFocusPolicy(Qt::StrongFocus);
    btnCancel->setFocusPolicy(Qt::StrongFocus);

    label->setAlignment(Qt::AlignCenter);
    label->setStyleSheet("QLabel{font-size:40px;}");
    btnOk->setStyleSheet("QPushButton{font-family:\"黑体\";font-size:32px;border:2px groove gray;border-radius:10px;padding:2px 4px;background-color:rgb(225,225,225);}");
    btnCancel->setStyleSheet("QPushButton{font-family:\"黑体\";font-size:32px;border:2px groove gray;border-radius:10px;padding:2px 4px;background-color:rgb(225,225,225);}");

    QGridLayout *grid = new QGridLayout(m_tmpDlgPopUp);
    grid->addWidget(label,0,0);
    grid->addWidget(btnOk,1,0);
    grid->addWidget(btnCancel,1,1);
    grid->setSpacing(10);
    grid->setMargin(30);

    label->setFixedSize(500,100);
    btnOk->setFixedSize(220,90);
    btnCancel->setFixedSize(220,90);

    connect(btnOk,&QPushButton::clicked,[=](){
        m_tmpDlgPopUp->close();
        DlgMain *parent = (DlgMain*)m_pParent;
        parent->DelFoodInfo(m_tmpDishName);
    });

    connect(btnCancel,&QPushButton::clicked,[=](){
        m_tmpDlgPopUp->close();
    });

    m_tmpDlgPopUp->exec();
}

void DlgEditDish::on_pushButton_curDishFeature_clicked()
{
    //获取当前菜的特征信息
    if(m_tmpDishName.length() <= 0){
        return;
    }
    ui->pushButton_curDishFeature->setDisabled(true);

    DlgMain *parent = (DlgMain*)m_pParent;
    parent->GetFoodInfoByName(m_tmpDishName);
}

void DlgEditDish::on_pushButton_CloseApp_clicked()
{
    DlgMain* parent = (DlgMain*)m_pParent;
    emit parent->sigExitApp();
}
