﻿#include "dlgeditmenu.h"
#include "ui_dlgeditmenu.h"

#include "dlgmain.h"

DlgEditMenu::DlgEditMenu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgEditMenu)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint | Qt::Dialog);
    this->setFixedSize(g_nscreenwidth, g_nscreenheight);
    this->setGeometry(g_FirstScreen);
    //
    m_pParent = (void*)parent;
    m_pCreateMenu = NULL;
    //
    connect(ui->listWidget_MenuCfg,&QListWidget::itemClicked,this,&DlgEditMenu::slotMenuCfgItemClicked);
    //
    ui->lineEdit_SearchlistWidgetNewMenu->setPlaceholderText(u8"搜索菜品");
    ui->lineEdit_SearchlistWidgetOriginalMenu->setPlaceholderText(u8"搜索菜品");
    connect(ui->lineEdit_SearchlistWidgetNewMenu,&QLineEdit::textChanged,this,&DlgEditMenu::slotSearchNewMenuChanged);
    connect(ui->lineEdit_SearchlistWidgetOriginalMenu,&QLineEdit::textChanged,this,&DlgEditMenu::slotSearchOriginalMenuChanged);
    //
//    ui->pushBtnCreateMenu->setFocusPolicy(Qt::NoFocus);
//    ui->pushBtnDeleteMenu->setFocusPolicy(Qt::NoFocus);
//    ui->pushBtnSetDefaultMenu->setFocusPolicy(Qt::NoFocus);
//    ui->pushBtnMove2Left->setFocusPolicy(Qt::NoFocus);
//    ui->pushBtnMove2Right->setFocusPolicy(Qt::NoFocus);
}

DlgEditMenu::~DlgEditMenu()
{
    delete ui;
}

void DlgEditMenu::slotAllMenus(QJsonArray array)
{
    //菜单列表--获取数据后---获取当前菜单的菜品信息
    ui->listWidget_MenuCfg->clear();
    for(int i=0; i<array.count(); i++){
        ui->listWidget_MenuCfg->addItem(array.at(i).toString());
    }
    //
    QString CurUseMenuName;
    QFile file("CurrentUseMenuName");
    if(file.open(QIODevice::ReadOnly)){
        CurUseMenuName = file.readAll().simplified();
        file.close();
    }
    if(CurUseMenuName.length() <= 0){
        CurUseMenuName = "全菜单";
    }
    //
    ui->label_CfgUsingMenuName->setText(QString("当前使用的菜单是:%1").arg(CurUseMenuName));
    ui->label_CfgNewMenuName->setText(CurUseMenuName);

    DlgMain* parent = (DlgMain*)m_pParent;
    parent->GetMenuInfo(CurUseMenuName);
}

void DlgEditMenu::slotMenusInfo(QString strMsg)
{
    //单机菜单列表--发送请求-获取该菜单的数据后
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(strMsg.toUtf8(), &error);
    QJsonObject obj = doc.object();
    QJsonArray array = obj.take("FoodNames").toArray();
    QJsonArray arrayAll = obj.take("AllFoodNames").toArray();
    ui->listWidget_NewMenu->clear();
    for(int i=0; i<array.count(); i++){
        ui->listWidget_NewMenu->addItem(array.at(i).toString());
    }

    ui->listWidget_OriginalMenu->clear();
    int nAllCount = 0;
    for(int i=0; i<arrayAll.count(); i++){
        QString name = arrayAll.at(i).toString();
        bool bH = false;
        for(int j=0; j<array.count(); j++){
            if(name == array.at(j).toString()){
                bH = true;
                break;
            }
        }
        if(!bH){
            nAllCount++;
            ui->listWidget_OriginalMenu->addItem(name);
        }
    }

    QString MeunName = obj.take("MenuName").toString();
    ui->label_CfgNewMenuName->setText(QString("%1(%2)").arg(MeunName).arg(array.count()));
    ui->label_CfgDefaultMenuName->setText(QString("全菜单(%1)").arg(nAllCount));
}

void DlgEditMenu::slotSetCurMenusNameEnd(QString MenuName)
{
    ui->label_CfgUsingMenuName->setText(QString("当前使用的菜单是:%1").arg(MenuName));
}

void DlgEditMenu::slotDelMenuByNameEnd(QString MenuName)
{
    DlgMain* parent = (DlgMain*)m_pParent;
    parent->GetAllMenus();
}

void DlgEditMenu::slotMenuCfgItemClicked(QListWidgetItem *item)
{
    //单机菜单列表操作
    m_tmpMenuCfg_MenuName = item->text();
    ui->label_CfgNewMenuName->setText(m_tmpMenuCfg_MenuName);

    DlgMain* parent = (DlgMain*)m_pParent;
    parent->GetMenuInfo(m_tmpMenuCfg_MenuName);
}

void DlgEditMenu::slotSearchNewMenuChanged(const QString &text)
{
    if(text.length() <1)
        return;
    int nRowCount = ui->listWidget_NewMenu->count();
    for(int i=0; i<nRowCount; i++){
        QString textOne = ui->listWidget_NewMenu->item(i)->text();
        if(textOne.contains(text)){
            QListWidgetItem *item = ui->listWidget_NewMenu->takeItem(i);
            ui->listWidget_NewMenu->insertItem(0, item);
            ui->listWidget_NewMenu->setCurrentRow(0);
        }
    }
}

void DlgEditMenu::slotSearchOriginalMenuChanged(const QString &text)
{
    if(text.length() <1)
        return;
    int nRowCount = ui->listWidget_OriginalMenu->count();
    for(int i=0; i<nRowCount; i++){
        QString textOne = ui->listWidget_OriginalMenu->item(i)->text();
        if(textOne.contains(text)){
            QListWidgetItem *item = ui->listWidget_OriginalMenu->takeItem(i);
            ui->listWidget_OriginalMenu->insertItem(0, item);
            ui->listWidget_OriginalMenu->setCurrentRow(0);
        }
    }
}

void DlgEditMenu::on_pushBtnSetDefaultMenu_clicked()
{
    //设为使用菜单
    DlgMain* parent = (DlgMain*)m_pParent;
    parent->SetCurrentUseMenu(m_tmpMenuCfg_MenuName);
}

void DlgEditMenu::on_pushBtnDeleteMenu_clicked()
{
    //删除选中菜单
    DlgMain* parent = (DlgMain*)m_pParent;
    parent->DelMenuByName(m_tmpMenuCfg_MenuName);
}

void DlgEditMenu::on_pushBtnCreateMenu_clicked()
{
    //新建菜单
    if(NULL != m_pCreateMenu){
        delete m_pCreateMenu;
        m_pCreateMenu = NULL;
    }
    m_pCreateMenu = new QDialog(this);
    m_pCreateMenu->setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);
    m_pCreateMenu->setFixedSize(600,400);

    QLineEdit *edit = new QLineEdit(m_pCreateMenu);
    edit->setPlaceholderText("请输入菜单名称");
    QPushButton *btnOk = new QPushButton("确 定", m_pCreateMenu);
    QPushButton *btnCancel = new QPushButton("取 消", m_pCreateMenu);

    edit->setAlignment(Qt::AlignCenter);
    edit->setStyleSheet("QLineEdit{font-size:40px;}");
    btnOk->setStyleSheet("QPushButton{font-family:\"黑体\";font-size:32px;border:2px groove gray;border-radius:10px;padding:2px 4px;background-color:rgb(225,225,225);}");
    btnCancel->setStyleSheet("QPushButton{font-family:\"黑体\";font-size:32px;border:2px groove gray;border-radius:10px;padding:2px 4px;background-color:rgb(225,225,225);}");

    btnOk->setFocus();

    QGridLayout *grid = new QGridLayout(m_pCreateMenu);
    grid->addWidget(edit,0,0);
    grid->addWidget(btnOk,1,0);
    grid->addWidget(btnCancel,1,1);
    grid->setSpacing(10);
    grid->setMargin(30);

    edit->setFixedSize(500,100);
    btnOk->setFixedSize(220,90);
    btnCancel->setFixedSize(220,90);

    connect(btnOk,&QPushButton::clicked,[=](){
        if(edit->text().isEmpty())
        {
            edit->setPlaceholderText("菜单名称不能为空！");
            return;
        }
        m_pCreateMenu->close();
        QString MenuName = edit->text();
        ui->listWidget_MenuCfg->addItem(MenuName);
    });

    connect(btnCancel,&QPushButton::clicked,[=](){
        m_pCreateMenu->close();
    });

    m_pCreateMenu->show();
}

void DlgEditMenu::on_pushBtnMove2Left_clicked()
{
    //全菜单往新菜单移动
    QString menuName = ui->label_CfgNewMenuName->text();//->setText(QString("%1(%2)").arg(MeunName).arg(array.count()));
    menuName = menuName.left(menuName.indexOf("("));
    if("全菜单" == menuName){
        return;
    }
    if(ui->listWidget_OriginalMenu->currentItem()
            && ui->listWidget_OriginalMenu->count() > 0
            && ui->listWidget_OriginalMenu->selectedItems().count() > 0){
        QString strOriginalMenuFoodName = ui->listWidget_OriginalMenu->currentItem()->text();
        ui->listWidget_NewMenu->insertItem(0, strOriginalMenuFoodName);
        ui->listWidget_NewMenu->setCurrentRow(0);

        ui->listWidget_OriginalMenu->takeItem(ui->listWidget_OriginalMenu->currentRow());
        //
        QString tmpChange_OriginalMenu = ui->label_CfgDefaultMenuName->text();
        tmpChange_OriginalMenu = tmpChange_OriginalMenu.left(tmpChange_OriginalMenu.indexOf("("));
        tmpChange_OriginalMenu = QString("%1(%2)").arg(tmpChange_OriginalMenu).arg(ui->listWidget_OriginalMenu->count());
        ui->label_CfgDefaultMenuName->setText(tmpChange_OriginalMenu);

        QString tmpChange_NewMenu = ui->label_CfgNewMenuName->text();
        tmpChange_NewMenu = tmpChange_NewMenu.left(tmpChange_NewMenu.indexOf("("));
        tmpChange_NewMenu = QString("%1(%2)").arg(tmpChange_NewMenu).arg(ui->listWidget_NewMenu->count());
        ui->label_CfgNewMenuName->setText(tmpChange_NewMenu);
        //
        QJsonArray array;
        int nRowCount = ui->listWidget_NewMenu->count();
        for(int i=0; i<nRowCount; i++){
            QString tmpText = ui->listWidget_NewMenu->item(i)->text();
            array.append(tmpText);
        }

        QJsonObject obj;
        obj.insert("Cmd", "editMenuContent");
        obj.insert("MenuName", menuName);
        obj.insert("FoodNames", array);
        QJsonDocument doc;
        doc.setObject(obj);
        DlgMain* parent = (DlgMain*)m_pParent;
        parent->EditMenuContent(QString(doc.toJson()));

        ///////////
//        QJsonObject objParentProcess;
//        objParentProcess.insert("Cmd", "editMenuContent");
//        objParentProcess.insert("MenuName", menuName);
//        QJsonDocument docParentProcess;
//        docParentProcess.setObject(objParentProcess);
//        QFile fileout;
//        fileout.open(stdout, QIODevice::WriteOnly);
//        fileout.write(docParentProcess.toJson());
//        fileout.close();
    }
}

void DlgEditMenu::on_pushBtnMove2Right_clicked()
{
    //新菜单移动到全菜单
    QString menuName = ui->label_CfgNewMenuName->text();//->setText(QString("%1(%2)").arg(MeunName).arg(array.count()));
    menuName = menuName.left(menuName.indexOf("("));
    if("全菜单" == menuName){
        return;
    }
    if(ui->listWidget_NewMenu->currentItem()
            && ui->listWidget_NewMenu->count() > 0
            && ui->listWidget_NewMenu->selectedItems().count() > 0){
        QString strNewMenuFoodName = ui->listWidget_NewMenu->currentItem()->text();
        ui->listWidget_OriginalMenu->insertItem(0, strNewMenuFoodName);
        ui->listWidget_OriginalMenu->setCurrentRow(0);

        ui->listWidget_NewMenu->takeItem(ui->listWidget_NewMenu->currentRow());
        //
        QString tmpChange_OriginalMenu = ui->label_CfgDefaultMenuName->text();
        tmpChange_OriginalMenu = tmpChange_OriginalMenu.left(tmpChange_OriginalMenu.indexOf("("));
        tmpChange_OriginalMenu = QString("%1(%2)").arg(tmpChange_OriginalMenu).arg(ui->listWidget_OriginalMenu->count());
        ui->label_CfgDefaultMenuName->setText(tmpChange_OriginalMenu);

        QString tmpChange_NewMenu = ui->label_CfgNewMenuName->text();
        tmpChange_NewMenu = tmpChange_NewMenu.left(tmpChange_NewMenu.indexOf("("));
        tmpChange_NewMenu = QString("%1(%2)").arg(tmpChange_NewMenu).arg(ui->listWidget_NewMenu->count());
        ui->label_CfgNewMenuName->setText(tmpChange_NewMenu);
        //
        QJsonArray array;
        int nRowCount = ui->listWidget_NewMenu->count();
        for(int i=0; i<nRowCount; i++){
            QString tmpText = ui->listWidget_NewMenu->item(i)->text();
            array.append(tmpText);
        }

        QJsonObject obj;
        obj.insert("Cmd", "editMenuContent");
        obj.insert("MenuName", menuName);
        obj.insert("FoodNames", array);
        QJsonDocument doc;
        doc.setObject(obj);
        DlgMain* parent = (DlgMain*)m_pParent;
        parent->EditMenuContent(QString(doc.toJson()));

        ///////
//        QJsonObject objParentProcess;
//        objParentProcess.insert("Cmd", "editMenuContent");
//        objParentProcess.insert("MenuName", menuName);
//        QJsonDocument docParentProcess;
//        docParentProcess.setObject(objParentProcess);
//        QFile fileout;
//        fileout.open(stdout, QIODevice::WriteOnly);
//        fileout.write(docParentProcess.toJson());
//        fileout.close();
    }
}

void DlgEditMenu::on_pushButton_CloseApp_clicked()
{
    DlgMain* parent = (DlgMain*)m_pParent;
    emit parent->sigExitApp();
}
