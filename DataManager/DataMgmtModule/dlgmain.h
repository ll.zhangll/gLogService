﻿#ifndef DLGMAIN_H
#define DLGMAIN_H

#include "commdef.h"
#include "netrequest.h"
#include "keyboard/keyboard.h"
#include "dlgeditdish.h"
#include "dlgeditmenu.h"
#include "dlgbill.h"
#include "dlgreport.h"


namespace Ui {
class DlgMain;
}

class DlgMain : public QDialog
{
    Q_OBJECT

public:
    explicit DlgMain(QWidget *parent = 0);
    ~DlgMain();
    //菜单管理界面
    void StartMenuCfg();
    //菜单模板管理界面
    void StartMenuTemplate();
    //账单流水界面
    void StartBill(uint logIntime);
    //报表界面
    void StartReport(uint logIntime);

    //////// 数据服务模块的请求实现
    ///菜品操作
    void GetMenuAll();//获取全部菜单基本信息
    void GetFoodEnergy(QString foodName);
    void SetFoodEnergy(QString msg);
    void SetFoodInfo(QString msg);
    void DelFoodInfo(QString foodName);
    void GetFoodInfoByName(QString foodName);
    void DelFoodInfoCurFea(QString fileName);
    ///菜单操作
    void GetAllMenus();
    void GetMenuInfo(QString menuName);
    void SetCurrentUseMenu(QString menuName);
    void DelMenuByName(QString menuName);
    void EditMenuContent(QString msg);
    ///账单操作
    void QueryBill(QString msg);
    void QueryTotalMoney(QString data);
    void QueryBillDetail(QString data);
    //报表操作
    void QueryReport(QString msg);
    void QueryReportQuery(QString msg);
    void ReportExport(QString msg);
    /////////////////
private:
    void Init();
    void ConnectTheWorld();
    void loadLayout();
    void loadUI();
    void UnInit();
    void ClearFoodDetailsList();

    NetRequest *m_netRequest;
    QThread m_Thread1;
    //
    DlgEditDish *m_pDlgEditDish;
    DlgEditMenu *m_pDlgEditMenu;
    //
    DlgBill     *m_pDlgBill;
    DlgReport   *m_pDlgReport;
signals:
    void sigExitApp();
    void sigPostThreadMsg(QString task);
    void sigMenuAll();
    void sigEnergy(QString);
    void sigFoodUrlArray(QJsonArray); //获取当前菜的所有图片的url
    void sigAllMenus(QJsonArray);
    void sigMenusInfo(QString strMsg);
    void sigSetCurMenusNameEnd(QString menuName);
    void sigDelMenuByNameEnd(QString menuName);
    void sigBillListResult(QString);
    void sigBillTotalMoney(QString);
    void sigBillDetail(QString);
    void sigReportLogsListResult(QString);
    void sigReportInfoListResult(QString);
    void sigReportQrCodeResult(QString);
private slots:
    //////////////// 解析线程中网络请求的结果并进行解析、逻辑
    void slotRecvThreadReplyMsg(QString msg);
    void slotExitApp();

private:
    Ui::DlgMain *ui;
};

#endif // DLGMAIN_H
