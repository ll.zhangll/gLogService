﻿#ifndef DLGEDITMENU_H
#define DLGEDITMENU_H

#include "commdef.h"

namespace Ui {
class DlgEditMenu;
}

class DlgEditMenu : public QDialog
{
    Q_OBJECT

public:
    explicit DlgEditMenu(QWidget *parent = 0);
    ~DlgEditMenu();

private:
    QDialog *m_pCreateMenu;
    QString m_tmpMenuCfg_MenuName;//listWidget_MenuCfg
    void *m_pParent;
public slots:
    void slotAllMenus(QJsonArray array);
    void slotMenusInfo(QString strMsg);
    void slotSetCurMenusNameEnd(QString MenuName);
    void slotDelMenuByNameEnd(QString MenuName);

private slots:
    void slotMenuCfgItemClicked(QListWidgetItem *item);
    void slotSearchNewMenuChanged(const QString &text);
    void slotSearchOriginalMenuChanged(const QString &text);
    void on_pushBtnSetDefaultMenu_clicked();
    void on_pushBtnDeleteMenu_clicked();
    void on_pushBtnCreateMenu_clicked();
    void on_pushBtnMove2Left_clicked();
    void on_pushBtnMove2Right_clicked();
    //
    void on_pushButton_CloseApp_clicked();

private:
    Ui::DlgEditMenu *ui;
};

#endif // DLGEDITMENU_H
