﻿#include "dlgreport.h"
#include "ui_dlgreport.h"
#include "dlgmain.h"
#include <QBuffer>
extern QString g_CurMasterQrCodeIp;

DlgReport::DlgReport(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgReport)
{
    ui->setupUi(this);

    m_pDlgHint = NULL;
    m_pParent = parent;
    this->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint | Qt::Dialog);
    this->setFixedSize(g_nscreenwidth, g_nscreenheight);
    this->setGeometry(g_FirstScreen);
    //////////////////////////
    ui->lineTimeReportFrom->installEventFilter(this);
    ui->lineTimeReportTo->installEventFilter(this);
    ui->lineTimeReportDate->installEventFilter(this);
    //////////////////////////////////////
    m_nFromChange = 0;
    m_tmpDisableSecond = 0;
    m_pSliderTime = new DlgSelector();
    connect(m_pSliderTime,&DlgSelector::sigClickedOk,this,&DlgReport::slotReportTimeChanged);
    /////////////////////////////////////////////
    ui->tableWidgetReport->viewport()->installEventFilter(this);
    ui->tableWidgetReport->verticalScrollBar()->installEventFilter(this);
    QHeaderView *BillHorHead = ui->tableWidgetReport->horizontalHeader();
    BillHorHead->setFixedHeight(70);
    BillHorHead->setDefaultAlignment(Qt::AlignCenter);
    BillHorHead->setHighlightSections(false);
    ui->tableWidgetReport->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableWidgetReport->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidgetReport->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableWidgetReport->verticalHeader()->setVisible(false);
    ui->tableWidgetReport->verticalScrollBar()->setVisible(false);
    ui->tableWidgetReport->setShowGrid(false);
//    ui->tableWidgetReport->setFocusPolicy(Qt::NoFocus);
//    ui->tableWidgetReport->horizontalHeader()->setFocusPolicy(Qt::NoFocus);
    ui->tableWidgetReport->setColumnCount(6);
    ui->tableWidgetReport->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Stretch);
    ui->tableWidgetReport->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);
    ui->tableWidgetReport->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Fixed);
    ui->tableWidgetReport->horizontalHeader()->setSectionResizeMode(3,QHeaderView::Fixed);
    ui->tableWidgetReport->setColumnWidth(2, 350);
    ui->tableWidgetReport->setColumnWidth(3, 350);
    ui->tableWidgetReport->horizontalHeader()->setSectionResizeMode(4,QHeaderView::Stretch);
    ui->tableWidgetReport->horizontalHeader()->setSectionResizeMode(5,QHeaderView::Stretch);
    QStringList tableHeaderBill;
    tableHeaderBill << tr(u8"编号") << tr(u8"操作员") << tr(u8"登入时间")
                    << tr(u8"登出时间") << tr(u8"总单数")
                    << tr(u8"总钱数");
    ui->tableWidgetReport->setHorizontalHeaderLabels(tableHeaderBill);
    //////////////////////////////
    QHeaderView *ReportHorHead = ui->tableWidgetMDReport->horizontalHeader();
    ReportHorHead->setFixedHeight(70);
    ReportHorHead->setDefaultAlignment(Qt::AlignCenter);
    ReportHorHead->setHighlightSections(false);
    ui->tableWidgetMDReport->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableWidgetMDReport->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidgetMDReport->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableWidgetMDReport->verticalHeader()->setVisible(false);
    ui->tableWidgetMDReport->verticalScrollBar()->setVisible(false);
    ui->tableWidgetMDReport->setShowGrid(false);
//    ui->tableWidgetMDReport->setFocusPolicy(Qt::NoFocus);
    ui->tableWidgetMDReport->setRowCount(6);
    ui->tableWidgetMDReport->setColumnCount(3);
    ui->tableWidgetMDReport->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Stretch);
    ui->tableWidgetMDReport->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Stretch);
    ui->tableWidgetMDReport->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Stretch);

    ui->tableWidgetMDReport->horizontalHeader()->setFixedHeight(80);
    ui->tableWidgetMDReport->horizontalHeader()->setDefaultAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    QFont font = ui->tableWidgetMDReport->horizontalHeader()->font();
    font.setPointSize(40);font.setBold(true); font.setFamily(u8"黑体");
    ui->tableWidgetMDReport->horizontalHeader()->setFont(font);
    QStringList tableHeader;
    tableHeader << tr(u8"收款类型") << tr(u8"单数") << tr(u8"金额");
    ui->tableWidgetMDReport->setHorizontalHeaderLabels(tableHeader);
    QTableWidgetItem *item = NULL;
    ui->tableWidgetMDReport->setRowHeight(0, 70);
    item = new QTableWidgetItem(u8"一卡通"); ui->tableWidgetMDReport->setItem(0, 0, item);  item->setTextAlignment(Qt::AlignCenter);
    item = new QTableWidgetItem("0");        ui->tableWidgetMDReport->setItem(0, 1, item);  item->setTextAlignment(Qt::AlignCenter);
    item = new QTableWidgetItem("0");        ui->tableWidgetMDReport->setItem(0, 2, item);  item->setTextAlignment(Qt::AlignCenter);

    ui->tableWidgetMDReport->setRowHeight(1, 70);
    item = new QTableWidgetItem(u8"虚拟卡"); ui->tableWidgetMDReport->setItem(1, 0, item);  item->setTextAlignment(Qt::AlignCenter);
    item = new QTableWidgetItem("0");        ui->tableWidgetMDReport->setItem(1, 1, item);  item->setTextAlignment(Qt::AlignCenter);
    item = new QTableWidgetItem("0");        ui->tableWidgetMDReport->setItem(1, 2, item);  item->setTextAlignment(Qt::AlignCenter);

    ui->tableWidgetMDReport->setRowHeight(2, 70);
    item = new QTableWidgetItem(u8"支付宝微信"); ui->tableWidgetMDReport->setItem(2, 0, item);  item->setTextAlignment(Qt::AlignCenter);
    item = new QTableWidgetItem("0");      ui->tableWidgetMDReport->setItem(2, 1, item);  item->setTextAlignment(Qt::AlignCenter);
    item = new QTableWidgetItem("0");      ui->tableWidgetMDReport->setItem(2, 2, item);  item->setTextAlignment(Qt::AlignCenter);

    ui->tableWidgetMDReport->setRowHeight(3, 70);
    item = new QTableWidgetItem(u8"现金"); ui->tableWidgetMDReport->setItem(3, 0, item);  item->setTextAlignment(Qt::AlignCenter);
    item = new QTableWidgetItem("0");      ui->tableWidgetMDReport->setItem(3, 1, item);  item->setTextAlignment(Qt::AlignCenter);
    item = new QTableWidgetItem("0");      ui->tableWidgetMDReport->setItem(3, 2, item);  item->setTextAlignment(Qt::AlignCenter);

    ui->tableWidgetMDReport->setRowHeight(4, 70);
    item = new QTableWidgetItem(u8"挂账"); ui->tableWidgetMDReport->setItem(4, 0, item);  item->setTextAlignment(Qt::AlignCenter);
    item = new QTableWidgetItem("0");      ui->tableWidgetMDReport->setItem(4, 1, item);  item->setTextAlignment(Qt::AlignCenter);
    item = new QTableWidgetItem("0");      ui->tableWidgetMDReport->setItem(4, 2, item);  item->setTextAlignment(Qt::AlignCenter);

    ui->tableWidgetMDReport->setRowHeight(5, 70);
    item = new QTableWidgetItem(u8"合计"); ui->tableWidgetMDReport->setItem(5, 0, item);  item->setTextAlignment(Qt::AlignCenter);
    item = new QTableWidgetItem("0");      ui->tableWidgetMDReport->setItem(5, 1, item);  item->setTextAlignment(Qt::AlignCenter);
    item = new QTableWidgetItem("0");      ui->tableWidgetMDReport->setItem(5, 2, item);  item->setTextAlignment(Qt::AlignCenter);
    //////////////////////////////////////////////////////////////////////////////
    ui->labelReportTitel->clear();
}

DlgReport::~DlgReport()
{
    delete ui;
}

void DlgReport::showDlgReport(uint LogInTime)
{
    QDateTime curTime = QDateTime::currentDateTime();
    ui->lineTimeReportDate->setText(curTime.toString("yyyy-MM-dd"));

    m_tmpReprotDMTime.setDate(QDate(curTime.date().year(),curTime.date().month(),curTime.date().day()));

    m_TmpDateTimeF = QDateTime::fromTime_t(LogInTime);
    ui->lineTimeReportFrom->setText(m_TmpDateTimeF.toString("yyyy-MM-dd"));
    m_TmpDateTimeT = QDateTime::currentDateTime();
    ui->lineTimeReportTo->setText(m_TmpDateTimeT.toString("yyyy-MM-dd"));
    while(0 != ui->tableWidgetReport->rowCount()){
        QTableWidgetItem *item = ui->tableWidgetReport->item(0,0); delete item; item = NULL;
        item = ui->tableWidgetReport->item(0,1); delete item; item = NULL;
        item = ui->tableWidgetReport->item(0,2); delete item; item = NULL;
        item = ui->tableWidgetReport->item(0,3); delete item; item = NULL;
        item = ui->tableWidgetReport->item(0,4); delete item; item = NULL;
        item = ui->tableWidgetReport->item(0,5); delete item; item = NULL;
        ui->tableWidgetReport->removeRow(0);
    }
    ui->tableWidgetReport->clearContents();
    m_bDownQuery = true;
    nCurBillCount = 0;

    this->show();
}

bool DlgReport::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == ui->lineTimeReportFrom) {
        if (QEvent::MouseButtonPress == event->type()) {
            m_pSliderTime->SetTime(m_TmpDateTimeF);
            m_pSliderTime->SetTitle("选择开始时间");
            m_pSliderTime->raise();
            m_pSliderTime->setVisible(true);
            m_nFromChange = 1;
        }
    }
    if (obj == ui->lineTimeReportTo) {
        if (QEvent::MouseButtonPress == event->type()) {
            m_pSliderTime->SetTitle("选择结束时间");
            m_pSliderTime->SetTime(m_TmpDateTimeT);
            m_pSliderTime->raise();
            m_pSliderTime->setVisible(true);
            m_nFromChange = 2;
        }
    }
    if (obj == ui->lineTimeReportDate) {
        if (QEvent::MouseButtonPress == event->type()) {
            ui->labelReportTitel->clear();
            m_pSliderTime->SetTime(QDateTime::currentDateTime());
            m_pSliderTime->SetTitle("选择获取报表日期");
            m_pSliderTime->raise();
            m_pSliderTime->setVisible(true);
            m_nFromChange = 3;
        }
    }
    if(obj == ui->tableWidgetReport->viewport()){
        if(QEvent::MouseButtonPress == event->type()){
            QMouseEvent *move = static_cast<QMouseEvent *>(event);
            m_oldy1 = move->pos().y();
        }
        if(QEvent::MouseMove == event->type()){
            QMouseEvent *move = static_cast<QMouseEvent *>(event);
            int newy1 = move->pos().y();
            int nCurrentVal = ui->tableWidgetReport->verticalScrollBar()->value();
            int nHeight = ui->tableWidgetReport->rowHeight(nCurrentVal);
            if (abs(newy1 - m_oldy1) >= nHeight){
                if(newy1 > m_oldy1) {
                    //up
                    ui->tableWidgetReport->verticalScrollBar()->setValue(nCurrentVal-1);
                }else if(newy1 < m_oldy1){
                    //down
                    ui->tableWidgetReport->verticalScrollBar()->setValue(nCurrentVal+1);
                    if(nCurBillCount > ui->tableWidgetReport->rowCount()){
                        if(m_bDownQuery){
                            m_bDownQuery = false;
                            nCurBillCount += 4;
                            QJsonObject obj;
                            obj.insert("Cmd", "action_query");
                            qint64 start = m_TmpDateTimeF.toTime_t();
                            qint64 end = m_TmpDateTimeT.toTime_t();
                            obj.insert("start_time", start);
                            obj.insert("end_time", end);
                            obj.insert("from", ui->tableWidgetReport->rowCount());
                            obj.insert("count", 4);
                            QJsonDocument doc;
                            doc.setObject(obj);
                            QString data = doc.toJson(QJsonDocument::Compact);
                            DlgMain *parent = (DlgMain*)m_pParent;
                            parent->QueryReport(data);
                        }
                    }
                }
                m_oldy1 = newy1;
            }
        }
    }
    return QDialog::eventFilter(obj, event);
}

void DlgReport::slotReportTimeChanged(QDateTime dateTime)
{
    if(1 == m_nFromChange) {
        m_TmpDateTimeF.setDate(QDate(dateTime.date().year(),dateTime.date().month(),dateTime.date().day()));
        m_TmpDateTimeF.setTime(QTime(0,0,0));
        ui->lineTimeReportFrom->setText(m_TmpDateTimeF.toString("yyyy-MM-dd"));
    }else if(2 == m_nFromChange) {
        m_TmpDateTimeT.setDate(QDate(dateTime.date().year(),dateTime.date().month(),dateTime.date().day()));
        m_TmpDateTimeT.setTime(QTime(23,59,59));
        ui->lineTimeReportTo->setText(m_TmpDateTimeT.toString("yyyy-MM-dd"));
    }else if(3 == m_nFromChange) {
        m_tmpReprotDMTime.setDate(QDate(dateTime.date().year(),dateTime.date().month(),dateTime.date().day()));
        ui->lineTimeReportDate->setText(m_tmpReprotDMTime.toString("yyyy-MM-dd"));
    }
}

void DlgReport::on_btnReportReturn_clicked()
{
    DlgMain* parent = (DlgMain*)m_pParent;
    emit parent->sigExitApp();
}

void DlgReport::on_btnReportQuery_clicked()
{
    if (QDateTime::currentMSecsSinceEpoch() - m_tmpDisableSecond < 500){
        return;
    }
    m_tmpDisableSecond = QDateTime::currentMSecsSinceEpoch();
    while(0 != ui->tableWidgetReport->rowCount()){
        QTableWidgetItem *item = ui->tableWidgetReport->item(0,0); delete item; item = NULL;
        item = ui->tableWidgetReport->item(0,1); delete item; item = NULL;
        item = ui->tableWidgetReport->item(0,2); delete item; item = NULL;
        item = ui->tableWidgetReport->item(0,3); delete item; item = NULL;
        item = ui->tableWidgetReport->item(0,4); delete item; item = NULL;
        item = ui->tableWidgetReport->item(0,5); delete item; item = NULL;
        ui->tableWidgetReport->removeRow(0);
    }
    ui->tableWidgetReport->clearContents();

    QJsonObject obj;
    obj.insert("Cmd", "action_query");
    m_TmpDateTimeF.setTime(QTime(0,0,0));
    qint64 start = m_TmpDateTimeF.toTime_t();
    m_TmpDateTimeT.setTime(QTime(23,59,59));
    qint64 end = m_TmpDateTimeT.toTime_t();
    obj.insert("start_time", start);
    obj.insert("end_time", end);
    obj.insert("from", 0);
    obj.insert("count", 13);
    QJsonDocument doc;
    doc.setObject(obj);
    QString data = doc.toJson(QJsonDocument::Compact);

    DlgMain *parent = (DlgMain*)m_pParent;
    parent->QueryReport(data);
}

void DlgReport::slotReportLogsListResult(QString data)
{
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8(), &error);
    if(QJsonParseError::NoError != error.error)
        return;

    QJsonObject obj = doc.object();
    QJsonArray array = obj.take("Datas").toArray();
    int nRowCount = ui->tableWidgetReport->rowCount();
    int nRevCount = array.size();
    nCurBillCount = 0;
    nCurBillCount = obj.take("Count").toInt();
    if(0 == nRevCount) {
        return;
    }
    ui->tableWidgetReport->setRowCount(nRowCount + nRevCount);
    for(int i=0; i<nRevCount; i++)
    {
        ui->tableWidgetReport->setRowHeight(nRowCount + i, 55);
        QJsonObject childObj = array.at(i).toObject();

        QTableWidgetItem *item = new QTableWidgetItem(QString::number(nRowCount + i + 1));
        item->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        ui->tableWidgetReport->setItem(nRowCount + i, 0, item);

        item = new QTableWidgetItem(childObj.take("Name").toString());
        item->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        ui->tableWidgetReport->setItem(nRowCount + i, 1, item);

        uint Intime = childObj.take("Intime").toInt();
        QDateTime datePayTime = QDateTime::fromTime_t(Intime);
        item = new QTableWidgetItem(datePayTime.toString("yyyy-MM-dd hh:mm:ss"));
        item->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        ui->tableWidgetReport->setItem(nRowCount + i, 2, item);

        uint Outtime = childObj.take("Outtime").toInt();
        datePayTime = QDateTime::fromTime_t(Outtime);
        item = new QTableWidgetItem(datePayTime.toString("yyyy-MM-dd hh:mm:ss"));
        item->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        ui->tableWidgetReport->setItem(nRowCount + i, 3, item);

        item = new QTableWidgetItem(QString::number(childObj.take("Totalamount").toInt()));
        item->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        ui->tableWidgetReport->setItem(nRowCount + i, 4, item);

        item = new QTableWidgetItem(QString::number((double)(childObj.take("Totalmoney").toInt()/100),'f',2));
        item->setTextAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
        ui->tableWidgetReport->setItem(nRowCount + i, 5, item);
    }
    m_bDownQuery = true;
}

void DlgReport::slotReportInfoListResult(QString data)
{
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8(), &error);
    QJsonObject obj = doc.object();
    QString typeplusam = obj.take("typeplusam").toString();
    //paytype  0初始化;1一卡通;2虚拟卡;3支付宝;4微信;5现金;6挂账;
    //1_amount1_money1,2_amount2_money2,3_amount3_money3,
    //4_amount4_money4,5_amount5_money5,6_amount6_money6

    //一卡通
    QString tm1 = typeplusam.section(",", 0, 0);
    QTableWidgetItem *item = ui->tableWidgetMDReport->item(0, 1);
    item->setText(QString::number(tm1.section("_", 1, 1).toInt()));
    item = ui->tableWidgetMDReport->item(0, 2);
    item->setText(QString::number((double)(tm1.section("_", 2, 2).toInt())/100,'f', 2));
    //虚拟卡
    tm1 = typeplusam.section(",", 1, 1);
    item = ui->tableWidgetMDReport->item(1, 1);
    item->setText(QString::number(tm1.section("_", 1, 1).toInt()));
    item = ui->tableWidgetMDReport->item(1, 2);
    item->setText(QString::number((double)(tm1.section("_", 2, 2).toInt())/100,'f', 2));
    //支付宝微信
    tm1 = typeplusam.section(",", 2, 2);
    item = ui->tableWidgetMDReport->item(2, 1);
    item->setText(QString::number(tm1.section("_", 1, 1).toInt()));
    item = ui->tableWidgetMDReport->item(2, 2);
    item->setText(QString::number((double)(tm1.section("_", 2, 2).toInt())/100,'f', 2));
    //现金
    tm1 = typeplusam.section(",", 4, 4);
    item = ui->tableWidgetMDReport->item(3, 1);
    item->setText(QString::number(tm1.section("_", 1, 1).toInt()));
    item = ui->tableWidgetMDReport->item(3, 2);
    item->setText(QString::number((double)(tm1.section("_", 2, 2).toInt())/100,'f', 2));
    //挂账
    tm1 = typeplusam.section(",", 5, 5);
    item = ui->tableWidgetMDReport->item(4, 1);
    item->setText(QString::number(tm1.section("_", 1, 1).toInt()));
    item = ui->tableWidgetMDReport->item(4, 2);
    item->setText(QString::number((double)(tm1.section("_", 2, 2).toInt())/100,'f', 2));
    //总计
    item = ui->tableWidgetMDReport->item(5, 1);
    item->setText(QString::number(obj.take("totalamount").toInt()));
    item = ui->tableWidgetMDReport->item(5, 2);
    item->setText(QString::number((double)obj.take("totalmoney").toInt()/100,'f', 2));
}

void DlgReport::slotReportQrCodeResult(QString data)
{
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8(), &error);
    QJsonObject obj = doc.object();
    QString Cmd = obj.take("Cmd").toString();
    if("report_imageUpload" == Cmd){
        QString qrCodeUrl = obj.take("QrCodeUrl").toString();
        qDebug() << qrCodeUrl.toUtf8().data();

        if(NULL != m_pDlgHint){
            delete m_pDlgHint;
            m_pDlgHint = NULL;
        }
        m_pDlgHint = new DlgHint(this);
        QNetworkAccessManager net;
        QEventLoop loop;
        QNetworkReply *netReply = net.get(QNetworkRequest(qrCodeUrl));
        QObject::connect(netReply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
        loop.exec();
        QPixmap pixmap;
        pixmap.loadFromData(netReply->readAll());
        m_pDlgHint->SetQrCode(pixmap);

        m_pDlgHint->exec();
    }
}

void DlgReport::on_btnReportQueryDay_clicked()
{
    //获取日报表
    QDateTime startTime;
    startTime.setDate(QDate(m_tmpReprotDMTime.date().year(),m_tmpReprotDMTime.date().month(),m_tmpReprotDMTime.date().day()));
    startTime.setTime(QTime(0,0,0));
    qint64 start = startTime.toTime_t();

    QDateTime endTime;
    endTime.setDate(QDate(m_tmpReprotDMTime.date().year(),m_tmpReprotDMTime.date().month(),m_tmpReprotDMTime.date().day()));
    endTime.setTime(QTime(23,59,59));
    qint64 end = endTime.toTime_t();

    QJsonObject obj;
    obj.insert("Cmd", "action_record_query");
    obj.insert("start_time", start);
    obj.insert("end_time", end);
    QJsonDocument doc;
    doc.setObject(obj);
    QString data = doc.toJson(QJsonDocument::Compact);

    DlgMain *parent = (DlgMain*)m_pParent;
    parent->QueryReportQuery(data);

    ui->labelReportTitel->setText(QString("%1年%2月%3日报表")
                                  .arg(m_tmpReprotDMTime.date().year())
                                  .arg(m_tmpReprotDMTime.date().month())
                                  .arg(m_tmpReprotDMTime.date().day()));
}

void DlgReport::on_btnReportQueryMonth_clicked()
{
    //获取月报表
    QDateTime startTime;
    startTime.setDate(QDate(m_tmpReprotDMTime.date().year(), m_tmpReprotDMTime.date().month(), 1));
    startTime.setTime(QTime(0,0,0));
    qint64 start = startTime.toTime_t();

    int maxDay = 30;
    int nCurMoney = m_tmpReprotDMTime.date().month();
    if (2 == nCurMoney){
        int year = m_tmpReprotDMTime.date().year();
        bool isLoopYear = (((0 == (year % 4)) && (0 != (year % 100))) || (0 == (year % 400)));
        if (isLoopYear) {
            maxDay = 29;
        } else {
            maxDay = 28;
        }
    }else if(1 == nCurMoney || 3 == nCurMoney|| 5 == nCurMoney|| 7 == nCurMoney|| 8 == nCurMoney|| 10 == nCurMoney|| 12 == nCurMoney){
        maxDay = 31;
    }
    QDateTime endTime;
    endTime.setDate(QDate(m_tmpReprotDMTime.date().year(), m_tmpReprotDMTime.date().month(), maxDay));
    endTime.setTime(QTime(23,59,59));
    qint64 end = endTime.toTime_t();

    QJsonObject obj;
    obj.insert("Cmd", "action_record_query");
    obj.insert("start_time", start);
    obj.insert("end_time", end);
    QJsonDocument doc;
    doc.setObject(obj);
    QString data = doc.toJson(QJsonDocument::Compact);

    DlgMain *parent = (DlgMain*)m_pParent;
    parent->QueryReportQuery(data);

    ui->labelReportTitel->setText(QString("%1年%2月报表")
                                  .arg(m_tmpReprotDMTime.date().year())
                                  .arg(m_tmpReprotDMTime.date().month()));
}

void DlgReport::on_btnReportExport_clicked()
{
    QPixmap tmpImg = ui->widget_back->grab();
    QJsonObject PostObj;
    PostObj.insert("Cmd", "report_imageUpload");
    PostObj.insert("MasterIp", g_CurMasterQrCodeIp);
    QByteArray baImg;
    QBuffer buf_Img(&baImg);
    buf_Img.open(QIODevice::WriteOnly);
    tmpImg.save(&buf_Img, "JPG");
    PostObj.insert("ImageData",QString(baImg.toBase64()));
    QJsonDocument SendDoc;
    SendDoc.setObject(PostObj);
    QByteArray data = SendDoc.toJson(QJsonDocument::Compact);
    DlgMain *parent = (DlgMain*)m_pParent;
    parent->ReportExport(QString(data));
}
