﻿#ifndef DLGHINT_H
#define DLGHINT_H

#include <QDialog>

namespace Ui {
class DlgHint;
}

class DlgHint : public QDialog
{
    Q_OBJECT

public:
    explicit DlgHint(QWidget *parent = 0);
    ~DlgHint();

    void SetQrCode(QPixmap &pixmap);
private slots:
    void on_btnReturn_clicked();

private:
    Ui::DlgHint *ui;
};

#endif // DLGHINT_H
