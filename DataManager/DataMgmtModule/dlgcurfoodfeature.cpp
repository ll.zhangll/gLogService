﻿#include "dlgcurfoodfeature.h"
#include "ui_dlgcurfoodfeature.h"
#include "dlgmain.h"

DlgCurFoodFeature::DlgCurFoodFeature(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgCurFoodFeature)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);
    this->setFixedSize(978,747);

    ui->label_addfoodshowImg->setScaledContents(true);
    ui->label_addfoodshowImg->installEventFilter(this);
    connect(this,&DlgCurFoodFeature::sigbLastPage,this,&DlgCurFoodFeature::bLastPage,Qt::QueuedConnection);//防止事件循环混乱
}

DlgCurFoodFeature::~DlgCurFoodFeature()
{
    delete ui;
}

void DlgCurFoodFeature::SetShowData(QJsonArray array, QString tmpDisName)
{
    m_CurArray = array;
    m_CurFoodName = tmpDisName;
    ui->label_addfoodshowTitle->setText(QString("特征管理-%1").arg(m_CurFoodName));
    FirstPage();
}

void DlgCurFoodFeature::SetPParent(void *parent)
{
    m_pParent = parent;
}

void DlgCurFoodFeature::FirstPage()
{
    ui->label_addfoodshowImg->clear();
    m_countNum = 0;
    int nCount = m_CurArray.size();
    QString strPageShow;
    if(nCount <= 0){
       strPageShow = QString("0/0");
    }else{
        strPageShow = QString("1/%1").arg(nCount);
        QString urlPic = QString("%1%2").arg(g_Url).arg(m_CurArray.at(m_countNum).toString());
        QNetworkAccessManager net;
        QEventLoop loop;
        QNetworkReply *netReply = net.get(QNetworkRequest(urlPic));
        QObject::connect(netReply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
        loop.exec();
        QPixmap pixmap;
        pixmap.loadFromData(netReply->readAll());
        ui->label_addfoodshowImg->setPixmap(pixmap);
    }
    ui->label_addfoodshowPage->setText(strPageShow);
}

void DlgCurFoodFeature::bLastPage(bool bLast)
{
    if(bLast){
        m_countNum--;
        if(m_countNum <= 0) {m_countNum=0;}
    }else{
        m_countNum++;
    }
    int nCount = m_CurArray.size();
    if(m_countNum > nCount-1) {
        m_countNum = nCount-1;
    }else{
        QString strPageShow = QString("%1/%2").arg(m_countNum+1).arg(nCount);
        ui->label_addfoodshowPage->setText(strPageShow);
        QString urlPic = QString("%1%2").arg(g_Url).arg(m_CurArray.at(m_countNum).toString());
        QNetworkAccessManager net;
        QEventLoop loop;
        QNetworkReply *netReply = net.get(QNetworkRequest(urlPic));
        QObject::connect(netReply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
        loop.exec();
        QPixmap pixmap;
        pixmap.loadFromData(netReply->readAll());
        ui->label_addfoodshowImg->setPixmap(pixmap);
    }
}

bool DlgCurFoodFeature::eventFilter(QObject *obj, QEvent *event)
{
    if(obj == ui->label_addfoodshowImg){
        if(QEvent::MouseButtonPress == event->type())
        {
            QMouseEvent *move = static_cast<QMouseEvent *>(event);
            m_xPressed = move->x();
            m_bIsSlide = false;
        }
        if(QEvent::MouseMove == event->type())
        {
            QMouseEvent *move = static_cast<QMouseEvent *>(event);
            int xOffset = move->pos().x() - m_xPressed;
            if (move->buttons() == Qt::LeftButton)
            {
                if(xOffset > 50 && !m_bIsSlide)
                {
//                    bLastPage(true);
                    emit sigbLastPage(true); // 需要经过事件循环过滤事件
                    m_bIsSlide = true;
                }
                else if(xOffset < -50 && !m_bIsSlide)
                {
//                    bLastPage(false);
                    emit sigbLastPage(false);
                    m_bIsSlide = true;
                }
            }
        }
    }
    return QDialog::eventFilter(obj,event);
}

void DlgCurFoodFeature::on_btnAddfoodFinish_clicked()
{
    this->close();
}

void DlgCurFoodFeature::on_btnAddfoodDeleteCur_clicked()
{
    DlgMain *parent = (DlgMain*)m_pParent;
    QString fileName = m_CurArray.at(m_countNum).toString();
    fileName = fileName.right(fileName.length() - fileName.lastIndexOf("/") - 1);
    parent->DelFoodInfoCurFea(fileName.left(fileName.length() - 4));
    //
    m_CurArray.removeAt(m_countNum);
    FirstPage();
}
