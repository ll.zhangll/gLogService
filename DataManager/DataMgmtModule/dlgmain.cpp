﻿#include "dlgmain.h"
#include "ui_dlgmain.h"
#include <QDebug>
QMap<QString, FoodDetails*> g_FoodDetailsList;

DlgMain::DlgMain(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgMain)
{
    ui->setupUi(this);
    //////////////////////
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::Dialog);
    this->setFixedSize(g_nscreenwidth, g_nscreenheight);
    this->setGeometry(g_FirstScreen);
    //////////////////网络请求线程：发送，接受消息均在此线程内，接收到的信息的解析在主线程中
    m_netRequest = new NetRequest();
    m_netRequest->moveToThread(&m_Thread1);
    connect(&m_Thread1, &QThread::finished, m_netRequest, &QObject::deleteLater);
    connect(this, &DlgMain::sigPostThreadMsg, m_netRequest, &NetRequest::doThreadWork);
    connect(m_netRequest, &NetRequest::SendReplyMessage, this, &DlgMain::slotRecvThreadReplyMsg);
    m_Thread1.start();

    //
    keyboard::Instance()->move(g_FirstScreen.x(), (g_nscreenheight - keyboard::Instance()->height()));
    keyboard::Instance()->setVisible(false);
    //
    Init();
}

DlgMain::~DlgMain()
{
    UnInit();
    m_Thread1.quit();
    if(m_Thread1.wait())
    {
        m_Thread1.terminate();
        m_Thread1.wait(1000);
    }
    delete ui;
}

void DlgMain::StartMenuCfg()
{
    GetMenuAll();
    m_pDlgEditDish->show();
}

void DlgMain::StartMenuTemplate()
{
    GetAllMenus();
    m_pDlgEditMenu->show();
}

void DlgMain::StartBill(uint logIntime)
{
    m_pDlgBill->showDlgBill(logIntime);
}

void DlgMain::StartReport(uint logIntime)
{
    m_pDlgReport->showDlgReport(logIntime);
}

void DlgMain::GetMenuAll()
{
    QJsonObject obj;
    obj.insert("Cmd", "queryMenuAll");
    QJsonDocument doc;
    doc.setObject(obj);
    emit sigPostThreadMsg(QString(doc.toJson()));
}

void DlgMain::GetFoodEnergy(QString foodName)
{
    QJsonObject obj;
    obj.insert("Cmd", "queryFoodEnergy");
    obj.insert("name", foodName);
    QJsonDocument doc;
    doc.setObject(obj);
    emit sigPostThreadMsg(QString(doc.toJson()));
}

void DlgMain::SetFoodEnergy(QString msg)
{
    emit sigPostThreadMsg(msg);
}

void DlgMain::SetFoodInfo(QString msg)
{
    emit sigPostThreadMsg(msg);
}

void DlgMain::DelFoodInfo(QString foodName)
{
    QJsonObject obj;
    obj.insert("Cmd", "delFoodInfo");
    obj.insert("name", foodName);
    QJsonDocument doc;
    doc.setObject(obj);
    emit sigPostThreadMsg(QString(doc.toJson()));
}

void DlgMain::GetFoodInfoByName(QString foodName)
{
    QJsonObject obj;
    obj.insert("Cmd", "queryFoodInfo");
    obj.insert("name", foodName);
    QJsonDocument doc;
    doc.setObject(obj);
    emit sigPostThreadMsg(QString(doc.toJson()));
}

void DlgMain::DelFoodInfoCurFea(QString fileName)
{
    QJsonObject obj;
    obj.insert("Cmd", "delFixFoodFea");
    obj.insert("FeaName", fileName);
    QJsonDocument doc;
    doc.setObject(obj);
    emit sigPostThreadMsg(QString(doc.toJson()));
}

void DlgMain::GetAllMenus()
{
    QJsonObject obj;
    obj.insert("Cmd", "queryAllMenus");
    QJsonDocument doc;
    doc.setObject(obj);
    emit sigPostThreadMsg(QString(doc.toJson()));
}

void DlgMain::GetMenuInfo(QString menuName)
{
    QJsonObject obj;
    obj.insert("Cmd", "queryMenuInfo");
    obj.insert("MenuName", menuName);
    QJsonDocument doc;
    doc.setObject(obj);
    emit sigPostThreadMsg(QString(doc.toJson()));
}

void DlgMain::SetCurrentUseMenu(QString menuName)
{
//    QJsonObject obj;
//    obj.insert("Cmd", "setCurrentMenu");
//    obj.insert("MenuName", menuName);
//    QJsonDocument doc;
//    doc.setObject(obj);
//    emit sigPostThreadMsg(QString(doc.toJson()));

    QFile file("CurrentUseMenuName");
    if(file.open(QIODevice::WriteOnly)){
        file.resize(0);
        file.write(menuName.toUtf8().simplified());
        file.close();
    }
    emit sigSetCurMenusNameEnd(menuName.simplified());
    //当设置当前菜单时,需要通知Robot重新获取一遍当前菜单的信息及注册
    QJsonObject obj;
    obj.insert("Cmd", "SetCurrentMenu");
    QJsonDocument doc;
    doc.setObject(obj);
    QFile fileout;
    fileout.open(stdout, QIODevice::WriteOnly);
    fileout.write(doc.toJson());
    fileout.close();
}

void DlgMain::DelMenuByName(QString menuName)
{
    QJsonObject obj;
    obj.insert("Cmd", "delMeun");
    obj.insert("MenuName", menuName);
    QJsonDocument doc;
    doc.setObject(obj);
    emit sigPostThreadMsg(QString(doc.toJson()));
}

void DlgMain::EditMenuContent(QString msg)
{
    emit sigPostThreadMsg(msg);
}

void DlgMain::QueryBill(QString msg)
{
    emit sigPostThreadMsg(msg);
}

void DlgMain::QueryTotalMoney(QString data)
{
    emit sigPostThreadMsg(data);
}

void DlgMain::QueryBillDetail(QString data)
{
    emit sigPostThreadMsg(data);
}

void DlgMain::QueryReport(QString msg)
{
    emit sigPostThreadMsg(msg);
}

void DlgMain::QueryReportQuery(QString msg)
{
    emit sigPostThreadMsg(msg);
}

void DlgMain::ReportExport(QString msg)
{
    emit sigPostThreadMsg(msg);
}

void DlgMain::Init()
{
    m_pDlgEditDish = new DlgEditDish(this);
    m_pDlgEditMenu = new DlgEditMenu(this);
    m_pDlgBill = new DlgBill(this);
    m_pDlgReport = new DlgReport(this);

    ConnectTheWorld();
    loadLayout();
    loadUI();
}

void DlgMain::ConnectTheWorld()
{
    //菜品管理
    connect(this,&DlgMain::sigMenuAll,m_pDlgEditDish,&DlgEditDish::slotMenuAll,Qt::QueuedConnection);
    connect(this,&DlgMain::sigEnergy,m_pDlgEditDish,&DlgEditDish::slotEnergy,Qt::QueuedConnection);
    connect(this,&DlgMain::sigFoodUrlArray,m_pDlgEditDish,&DlgEditDish::slotFoodUrlArray,Qt::QueuedConnection);
    //菜单管理
    connect(this,&DlgMain::sigAllMenus,m_pDlgEditMenu,&DlgEditMenu::slotAllMenus,Qt::QueuedConnection);
    connect(this,&DlgMain::sigMenusInfo,m_pDlgEditMenu,&DlgEditMenu::slotMenusInfo,Qt::QueuedConnection);
    connect(this,&DlgMain::sigSetCurMenusNameEnd,m_pDlgEditMenu,&DlgEditMenu::slotSetCurMenusNameEnd,Qt::QueuedConnection);
    connect(this,&DlgMain::sigDelMenuByNameEnd,m_pDlgEditMenu,&DlgEditMenu::slotDelMenuByNameEnd,Qt::QueuedConnection);
    //账单流水管理
    connect(this,&DlgMain::sigBillListResult,m_pDlgBill,&DlgBill::slotBillListResult,Qt::QueuedConnection);
    connect(this,&DlgMain::sigBillTotalMoney,m_pDlgBill,&DlgBill::slotBillTotalMoney,Qt::QueuedConnection);
    connect(this,&DlgMain::sigBillDetail,m_pDlgBill,&DlgBill::slotBillDetail,Qt::QueuedConnection);
    //报表
    connect(this,&DlgMain::sigReportLogsListResult,m_pDlgReport,&DlgReport::slotReportLogsListResult,Qt::QueuedConnection);
    connect(this,&DlgMain::sigReportInfoListResult,m_pDlgReport,&DlgReport::slotReportInfoListResult,Qt::QueuedConnection);
    connect(this,&DlgMain::sigReportQrCodeResult,m_pDlgReport,&DlgReport::slotReportQrCodeResult,Qt::QueuedConnection);
    //退出
    connect(this,&DlgMain::sigExitApp,this,&DlgMain::slotExitApp,Qt::QueuedConnection);
}

void DlgMain::loadLayout()
{

}

void DlgMain::loadUI()
{

}

void DlgMain::UnInit()
{

}

void DlgMain::ClearFoodDetailsList()
{
    QMap<QString, FoodDetails*>::const_iterator i = g_FoodDetailsList.constBegin();
    while (i != g_FoodDetailsList.constEnd()) {
        FoodDetails *p = i.value();
        delete p; p = NULL;

        ++i;
    }
    g_FoodDetailsList.clear();
}

void DlgMain::slotRecvThreadReplyMsg(QString msg)
{
    qDebug()<<"查询返回结果"<<msg;
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(msg.toUtf8(), &error);
    QJsonObject obj = doc.object();
    QString Cmd = obj.take("Cmd").toString();
    if("queryMenuAll" == Cmd){
        ClearFoodDetailsList();
        QJsonArray array = obj.take("FoodInfos").toArray();
        for(int i=0; i<array.count(); i++){
            QJsonObject Item = array.at(i).toObject();
            FoodDetails *p = new FoodDetails();
            p->Id = i+1;
            p->nPrice = Item.take("Price").toInt();
            p->AnotherName = Item.take("OtherName").toString();
            g_FoodDetailsList.insert(Item.take("Name").toString(), p);
            emit sigMenuAll();
        }
    }else if("queryFoodEnergy" == Cmd){
        emit sigEnergy(msg);
    }else if("delFoodInfo" == Cmd){
        //删除成功后，再次获取所有菜的信息
        GetMenuAll();
    }else if("queryFoodInfo" == Cmd){
        QJsonArray array = obj.take("FoodImagesUrl").toArray();
        emit sigFoodUrlArray(array);
    }else if("delFixFoodFea" == Cmd){
        //获取删除固定菜品特征的结果
    }else if("queryAllMenus" == Cmd){
        //获取所有菜菜单的名称
        emit sigAllMenus(obj.take("Menus").toArray());
    }else if("queryMenuInfo" == Cmd){
        emit sigMenusInfo(msg);
    }else if("setCurrentMenu" == Cmd){
//        emit sigSetCurMenusNameEnd(obj.take("CurrentMenuName").toString()); //弃用
    }else if("delMeun" == Cmd){
        emit sigDelMenuByNameEnd(obj.take("MenuName").toString());
    }else if("bill_query" == Cmd){
        emit sigBillListResult(msg);
    }else if("bill_countmoney" == Cmd){
        emit sigBillTotalMoney(msg);
    }else if("bill_detail" == Cmd){
        emit sigBillDetail(msg);
    }else if("action_query" == Cmd){
        emit sigReportLogsListResult(msg);
    }else if("action_record_query" == Cmd){
        emit sigReportInfoListResult(msg);
    }else if("report_imageUpload" == Cmd){
        emit sigReportQrCodeResult(msg);
    }
}

void DlgMain::slotExitApp()
{
    this->close();
    QApplication::instance()->exit();
}
