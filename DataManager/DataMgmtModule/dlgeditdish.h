﻿#ifndef DLGEDITDISH_H
#define DLGEDITDISH_H

#include "commdef.h"
#include "dlgcurfoodfeature.h"

namespace Ui {
class DlgEditDish;
}

class DlgEditDish : public QDialog
{
    Q_OBJECT

public:
    explicit DlgEditDish(QWidget *parent = 0);
    ~DlgEditDish();

    QString m_tmpDishName; //菜单管理 当前点中的菜单的名字
    void *m_pParent;

private:
    QDialog *m_tmpTmpPop;
    QDialog *m_tmpDlgPopUp;
    //当前菜品的特征库
    DlgCurFoodFeature *m_tmpDlgCurFoodFeaMgmt;
protected:
    int m_oldy1;
    bool eventFilter(QObject *obj, QEvent *event);
public slots:
    void slotMenuAll();
    void slotEnergy(QString data);
    void slotFoodUrlArray(QJsonArray array);
    void slotListWidgetDishCfgitemClicked(QListWidgetItem *item);
    void slotSearchDishtextChanged(const QString &text);
private slots:
    void on_pushButton_curDishSave_clicked();
    void on_pushButton_deleteCurDish_clicked();
    void on_pushButton_curDishFeature_clicked();

    void on_pushButton_CloseApp_clicked();

private:
    Ui::DlgEditDish *ui;
};

#endif // DLGEDITDISH_H
