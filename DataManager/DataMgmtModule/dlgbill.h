﻿#ifndef DLGBILL_H
#define DLGBILL_H

#include "commdef.h"
#include "timewidget/mytimedialog.h"
#include <QTableWidgetItem>

namespace Ui {
class DlgBill;
}

class DlgBill : public QDialog
{
    Q_OBJECT

public:
    explicit DlgBill(QWidget *parent = 0);
    ~DlgBill();

    void showDlgBill(uint LogInTime);
private:
    MyTimeDialog *m_pSliderTime;
    int   m_nFromChange;//1 frome 2 to
    QDateTime m_TmpDateTimeF; //即登陆的时间
    QDateTime m_TmpDateTimeT;
    int m_oldy1;
    bool m_bDownQuery;
    int nCurBillCount;
    void *m_pParent;
    qint64 m_tmpDisableSecond;
    bool eventFilter(QObject *obj, QEvent *event);
public slots:
    void slotBillListResult(QString data);
    void slotBillTotalMoney(QString data);
    void slotBillDetail(QString data);
    void slotBillTimeChanged(QDateTime dateTime);
private slots:
    void slotBillItemClicked(QTableWidgetItem *item);
    void on_btnBillReturn_clicked();

    void on_btnBillQuery_clicked();

private:
    Ui::DlgBill *ui;
};

#endif // DLGBILL_H
