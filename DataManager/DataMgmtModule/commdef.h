﻿#ifndef COMMDEF_H
#define COMMDEF_H

#include <QDialog>
#include <QNetworkAccessManager>
#include <QTextCodec>
#include <QTimer>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QNetworkReply>
#include <QFile>
#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDebug>
#include <QThread>
#include <QMutex>
#include <QEvent>
#include <QMouseEvent>
#include <QScrollBar>
#include <QListWidgetItem>
#include <QBoxLayout>
#include <QDateTime>
extern quint16 g_nscreenwidth;
extern quint16 g_nscreenheight;
extern QRect g_FirstScreen;
extern QString g_Url;

//菜品字典
class FoodDetails{
public:
    FoodDetails() { Id = 0; nPrice = 0; AnotherName = ""; }
    ~FoodDetails() { }
    int Id; // 菜品编号
    int nPrice; // 菜品价格（分）
    QString AnotherName; // 菜品别名
};

extern QMap<QString, FoodDetails*> g_FoodDetailsList;

#endif // COMMDEF_H
