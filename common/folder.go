package common

import (
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
)

//判断文件或文件夹是否存在
func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

//递归获取所有文件夹下的文件
func GetFolderAllFiles(path string, s []string) ([]string, error) {
	rd, err := ioutil.ReadDir(path)
	if err != nil {
		return s, err
	}

	for _, fi := range rd {
		if fi.IsDir() {
			fullDir := path + "/" + fi.Name()
			s, err = GetFolderAllFiles(fullDir, s)
			if err != nil {
				return s, err
			}
		} else {
			fullName := path + "/" + fi.Name()
			s = append(s, fullName)
		}
	}

	return s, nil
}

//获取指定文件夹下的文件,去掉后缀的文件名,条件是.food
func GetFolderFilesNameBaseonFood(folder string, s []string) ([]string, error) {
	rd, err := ioutil.ReadDir(folder)
	if err != nil {
		return s, err
	}

	for _, fi := range rd {
		if fi.IsDir() {
			//
		} else {
			fileSuffix := path.Ext(fi.Name())
			if ".food" == fileSuffix {
				fileNameOnly := strings.TrimSuffix(fi.Name(), fileSuffix)
				s = append(s, fileNameOnly)
			}
		}
	}

	return s, nil
}

//获取指定文件夹下的文件,去掉后缀的文件名,条件是.jpg .fea
func GetFolderFilesNameBaseonJPGFea(folder string, s []string) ([]string, error) {
	rd, err := ioutil.ReadDir(folder)
	if err != nil {
		return s, err
	}

	for _, fi := range rd {
		if fi.IsDir() {
			//
		} else {
			fileSuffix := path.Ext(fi.Name())
			if ".jpg" == fileSuffix {
				fileNameOnly := strings.TrimSuffix(fi.Name(), fileSuffix)
				s = append(s, fileNameOnly)
			}
		}
	}

	return s, nil
}

// 删除该目录及目录下所有的文件及文件夹
func RemoveAllContents(dir string) error {
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		os.RemoveAll(filepath.Join(dir, name))
	}
	os.RemoveAll(dir)
	return nil
}

//根据部分名字找到文件夹中的文件全名
func GetFilePathBaseonName(folder string, name string) (string, error) {
	var s string
	rd, err := ioutil.ReadDir(folder)
	if err != nil {
		return s, err
	}

	for _, fi := range rd {
		if fi.IsDir() {
			//
		} else {
			fileSuffix := path.Ext(fi.Name())
			if ".food" == fileSuffix {
				nameslist := strings.Split(fi.Name(), "-")
				if name == nameslist[0] {
					s = fi.Name()
				}
			}
		}
	}

	return s, nil
}

//获取指定文件夹下的文件,去掉后缀的文件名,条件是.syn
func GetFolderFilesNameBaseonSyn(folder string, s []string) ([]string, error) {
	rd, err := ioutil.ReadDir(folder)
	if err != nil {
		return s, err
	}

	for _, fi := range rd {
		if fi.IsDir() {
			//
		} else {
			fileSuffix := path.Ext(fi.Name())
			if ".syn" == fileSuffix {
				// fileNameOnly := strings.TrimSuffix(fi.Name(), fileSuffix)
				// s = append(s, fileNameOnly)
				s = append(s, fi.Name())
			}
		}
	}

	return s, nil
}

//获取所有的菜单
func GetAllMenusName(folder string, s []string) ([]string, error) {

	s = append(s, "全菜单")

	rd, err := ioutil.ReadDir(folder)
	if err != nil {
		return s, err
	}

	for _, fi := range rd {
		if fi.IsDir() {
			//
		} else {
			fileSuffix := path.Ext(fi.Name())
			if ".json" == fileSuffix {
				fileNameOnly := strings.TrimSuffix(fi.Name(), fileSuffix)
				s = append(s, fileNameOnly)
			}
		}
	}

	return s, nil
}

//获取全菜单的所有菜名
func GetfullMenuFoods(folder string, s []string) ([]string, error) {
	rd, err := ioutil.ReadDir(folder)
	if err != nil {
		return s, err
	}

	for _, fi := range rd {
		if fi.IsDir() {
			//
		} else {
			fileSuffix := path.Ext(fi.Name())
			if ".food" == fileSuffix {
				fileNameOnly := strings.TrimSuffix(fi.Name(), fileSuffix)
				s = append(s, strings.Split(fileNameOnly, "-")[0])
			}
		}
	}

	return s, nil
}

//判断 foodInfo 中是否存在以 name 为首的文件，如果存在则反出该名称
func GetFoodInfoFolderName(folder string, name string) string {
	result := ""
	rd, err := ioutil.ReadDir(folder)
	if err != nil {
		return result
	}

	for _, fi := range rd {
		if fi.IsDir() {
			//
		} else {
			fileSuffix := path.Ext(fi.Name())
			if ".food" == fileSuffix {
				fileNameOnly := strings.TrimSuffix(fi.Name(), fileSuffix)
				if name == strings.Split(fileNameOnly, "-")[0] {
					result = fileNameOnly
				}
			}
		}
	}

	return result
}
